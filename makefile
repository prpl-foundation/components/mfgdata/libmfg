include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C pkgconfig all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C pkgconfig clean

install: all
	$(INSTALL) -D -p -m 644 src/libmfg.so $(DEST)/usr/lib/libmfg.so
	$(INSTALL) -D -p -m 644 include/mfg_api.h $(DEST)/include/mfg_api.h
	$(INSTALL) -D -p -m 0644 pkgconfig/libmfg.pc $(PKG_CONFIG_LIBDIR)/libmfg.pc

package: all
	$(INSTALL) -D -p -m 644 src/libmfg.so $(PKGDIR)/usr/lib/libmfg.so
	$(INSTALL) -D -p -m 644 include/mfg_api.h $(PKGDIR)/include/mfg_api.h
	$(INSTALL) -D -p -m 0644 pkgconfig/libmfg.pc $(PKGDIR)$(PKG_CONFIG_LIBDIR)/libmfg.pc
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	mkdir -p output/doc
	VERSION=$(VERSION) doxygen doc/lib_mfg.doxy


test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test