/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "libmfg_mock.h"
#include <cmocka.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static bool mfg_entries_containsMfgEntryName_success = true;
static bool parse_dtb_success = true;
static bool mfg_entries_getMfgEntryByName_success = true;
static bool mfg_init_success = true;

int __real_mfg_entries_containsMfgEntryName(struct mfg_entry* head, const char* name);
int __real_parse_dtb(void* fdt, struct mfg_entry** head);
struct mfg_entry* __real_mfg_entries_getMfgEntryByName(struct mfg_entry* head, const char* name);
int __real_mfg_init(struct mfg_entry** head);

int __wrap_mfg_entries_containsMfgEntryName(struct mfg_entry* head, const char* name) {
    if(!mfg_entries_containsMfgEntryName_success) {
        return 1;
    } else {
        return __real_mfg_entries_containsMfgEntryName(head, name);
    }
}

int __wrap_parse_dtb(void* fdt, struct mfg_entry** head) {
    if(!parse_dtb_success) {
        return -1;
    } else {
        return __real_parse_dtb(fdt, head);
    }
}

struct mfg_entry* __wrap_mfg_entries_getMfgEntryByName(struct mfg_entry* head, const char* name) {
    if(!mfg_entries_getMfgEntryByName_success) {
        return NULL;
    } else {
        return __real_mfg_entries_getMfgEntryByName(head, name);
    }
}

int __wrap_mfg_init(struct mfg_entry** head) {
    if(!mfg_init_success) {
        return MFG_ERR_IO;
    } else {
        return __real_mfg_init(head);
    }
}

void mock_set_mfg_entries_containsMfgEntryName_success(bool success) {
    mfg_entries_containsMfgEntryName_success = success;
}

void mock_set_parse_dtb_success(bool success) {
    parse_dtb_success = success;
}
void mock_set_mfg_entries_getMfgEntryByName_success(bool success) {
    mfg_entries_getMfgEntryByName_success = success;
}

void mock_set_mfg_init_success(bool success) {
    mfg_init_success = success;
}
