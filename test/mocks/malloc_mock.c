/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "malloc_mock.h"
#include "errno.h"
#include <cmocka.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static bool malloc_success = true;
static bool malloc_first_fail = true;
static bool calloc_success = true;
static bool strncpy_success = true;
static bool open_success = true;
static bool read_success = true;
static bool lseek_success = true;
static bool lseek_correct_offset = true;
static bool strncmp_success = true;

void* __real_malloc(size_t size);
void* __real_calloc(size_t size);
void* __real_strncpy(char* dest, const char* src, size_t n);
int __real_open(size_t size);
ssize_t __real_read(int fd, void* buf, size_t count);
off_t __real_lseek(int fd, off_t offset, int whence);
int __real_strncmp (const char* str1, const char* str2, size_t num);

// Mock malloc function
void* __wrap_malloc(size_t size) {
    // Check if we should simulate malloc failure (as specified by the test case)
    if(!malloc_success) {
        if(!malloc_first_fail) {
            static int fail_cntr = 0;
            int fail_at = 0;

            fail_at = mock_type(int);
            ++fail_cntr;

            if((fail_at > 0) && (fail_cntr == fail_at)) {
                errno = ENOMEM;
                fail_cntr = 0;
                return NULL;            // Simulate malloc failure
            }
            return __real_malloc(size); // Use the real malloc function
        }
        errno = ENOMEM;
        return NULL;
    } else {
        return __real_malloc(size); // Use the real malloc function
    }
}

// Mock calloc function
void* __wrap_calloc(size_t size) {
    // Check if we should simulate calloc failure (as specified by the test case)
    if(!calloc_success) {
        static int fail_cntr = 0;
        int fail_at = 0;

        fail_at = mock_type(int);
        ++fail_cntr;

        if((fail_at > 0) && (fail_cntr == fail_at)) {
            fail_cntr = 0;
            errno = ENOMEM;
            return NULL;            // Simulate calloc failure
        }
        return __real_calloc(size); // Use the real calloc function
    } else {
        return __real_calloc(size); // Use the real calloc function
    }
}

// Mock strncpy function
void* __wrap_strncpy(char* dest, const char* src, size_t n) {
    if(!strncpy_success) {
        return "";
    } else {
        return __real_strncpy(dest, src, n); // Use the real strncpy function
    }
}

// Mock open function
int __wrap_open(size_t size) {
    // Check if we should simulate open failure (as specified by the test case)
    if(!open_success) {
        errno = ENOENT;
        return -1;                // Simulate open failure
    } else {
        return __real_open(size); // Use the real open function
    }
}

// Mock read function
ssize_t __wrap_read(int fd, void* buf, size_t count) {
    // Check if we should simulate open failure (as specified by the test case)
    if(!read_success) {
        static int fail_cntr_read = 0;
        int fail_at_read = 0;

        fail_at_read = mock_type(int);
        ++fail_cntr_read;

        if((fail_at_read > 0) && (fail_cntr_read >= fail_at_read)) {
            fail_cntr_read = 0;
            errno = EIO;
            return -1;                      // Simulate read failure
        }
        return __real_read(fd, buf, count); // Use the real read function
    } else {
        return __real_read(fd, buf, count); // Use the real read function
    }
}

// Mock lseek function
off_t __wrap_lseek(int fd, off_t offset, int whence) {
    // Check if we should simulate lseek failure (as specified by the test case)
    if(!lseek_success) {
        errno = EBADF;
        return -1;                                     // Simulate lseek failure
    } else if(!lseek_correct_offset) {
        return __real_lseek(fd, offset + 20L, whence); // Set the wrong offset
    } else {
        return __real_lseek(fd, offset, whence);       // Use the real lseek function
    }
}

int __wrap_strncmp (const char* str1, const char* str2, size_t num) {
    // Check if we should simulate strcmp failure (as specified by the test case)
    if(!strncmp_success) {
        return 1;                               // Simulate strcmp with strings not the same
    } else {
        return __real_strncmp(str1, str2, num); // Use the real strcmp function
    }
}

void mock_set_malloc_success(bool success) {
    malloc_success = success;
}

void mock_set_malloc_first_fail(bool success) {
    malloc_first_fail = success;
}

void mock_set_calloc_success(bool success) {
    calloc_success = success;
}

void mock_set_strncpy_success(bool success) {
    strncpy_success = success;
}

void mock_set_open_success(bool success) {
    open_success = success;
}

void mock_set_read_success(bool success) {
    read_success = success;
}

void mock_set_lseek_success(bool success) {
    lseek_success = success;
}

void mock_set_lseek_correct_offset(bool success) {
    lseek_correct_offset = success;
}

void mock_set_strncmp_success(bool success) {
    strncmp_success = success;
}
