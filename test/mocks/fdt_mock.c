/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "fdt_mock.h"
#include "mfg_defs.h"
#include "errno.h"
#include <libfdt.h>
#include <cmocka.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static bool fdt_check_header_success = true;
static bool fdt_subnode_offset_success = true;
static bool fdt_first_subnode_success = true;
static bool fdt_get_name_success = true;
static bool fdt_getprop_success = true;
static bool fcntl_success = true;

int __real_fdt_check_header(size_t size);
int __real_fdt_subnode_offset(size_t size);
int __real_fdt_first_subnode(size_t size);
int __real_fdt_get_name(size_t size);
void* __real_fdt_getprop(const void* fdt, int nodeoffset, const char* name, int* lenp);
int __real_fcntl(int fd, int cmd, ...);

int __wrap_fdt_check_header(size_t size) {
    // Check if we should simulate fdt_check_header failure (as specified by the test case)
    if(!fdt_check_header_success) {
        return -1;                            // Simulate fdt_check_header failure
    } else {
        return __real_fdt_check_header(size); // Use the real fdt_check_header function
    }
}

int __wrap_fdt_subnode_offset(size_t size) {
    // Check if we should simulate fdt_subnode_offset failure (as specified by the test case)
    if(!fdt_subnode_offset_success) {
        return -1;                              // Simulate fdt_subnode_offset failure
    } else {
        return __real_fdt_subnode_offset(size); // Use the real fdt_subnode_offset function
    }
}

int __wrap_fdt_first_subnode(size_t size) {
    // Check if we should simulate fdt_first_subnode failure (as specified by the test case)
    if(!fdt_first_subnode_success) {
        return MFG_ERR_NOENT;                  // Simulate fdt_first_subnode failure
    } else {
        return __real_fdt_first_subnode(size); // Use the real fdt_first_subnode function
    }
}

int __wrap_fdt_get_name(size_t size) {
    // Check if we should simulate fdt_get_name failure (as specified by the test case)
    if(!fdt_get_name_success) {
        return MFG_ERR_NOENT;             // Simulate fdt_get_name failure
    } else {
        return __real_fdt_get_name(size); // Use the real fdt_get_name function
    }
}

void* __wrap_fdt_getprop(UNUSED const void* fdt, UNUSED int nodeoffset, UNUSED const char* name, UNUSED int* lenp) {
    if(!fdt_getprop_success) {
        static int fail_cntr = 0;
        int fail_at = 0;

        fail_at = mock_type(int);
        ++fail_cntr;

        if((fail_at > 0) && (fail_cntr == fail_at)) {
            fail_cntr = 0;
            return NULL;                                        // Simulate fdt_getprop malloc failure
        }
        return __real_fdt_getprop(fdt, nodeoffset, name, lenp); // Use the real fdt_getprop function
    } else {
        return __real_fdt_getprop(fdt, nodeoffset, name, lenp); // Use the real fdt_getprop function
    }
}

int __wrap_fcntl(int fd, int cmd, ...) {
    va_list args;
    va_start(args, cmd);

    // Check if we should simulate fcntl failure (as specified by the test case)
    if(!fcntl_success) {
        va_end(args);
        errno = EACCES;
        return -1;
    } else {
        struct flock* fl = va_arg(args, struct flock*);
        va_end(args);
        return __real_fcntl(fd, cmd, fl);
    }
}

void mock_set_fdt_check_header_success(bool success) {
    fdt_check_header_success = success;
}

void mock_set_fdt_subnode_offset_success(bool success) {
    fdt_subnode_offset_success = success;
}

void mock_set_fdt_first_subnode_success(bool success) {
    fdt_first_subnode_success = success;
}

void mock_set_fdt_get_name_success(bool success) {
    fdt_get_name_success = success;
}

void mock_set_fdt_getprop_success(bool success) {
    fdt_getprop_success = success;
}

void mock_set_fcntl_success(bool success) {
    fcntl_success = success;
}
