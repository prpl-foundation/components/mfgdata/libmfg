/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <cmocka.h>
#include <libfdt.h>
#include <fdt.h>
#include <libfdt_env.h>

#include "test_libmfg.h"
#include "mfg_api.h"
#include "mfg_defs.h"
#include "malloc_mock.h"
#include "fdt_mock.h"
#include "libmfg_mock.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

int parse_dtb(void* fdt, struct mfg_entry** head);

int test_libmfg_setup(UNUSED void** state) {
    return 0;
}

int test_libmfg_teardown(UNUSED void** state) {
    return 0;
}

static void check_values_mfgdtb(struct mfg_entry* head) {
    struct mfg_entry* current = NULL;

    // Check that the head is not NULL. Make sure that all the fields are added to the list
    assert_non_null(head);

    // Check if all the values from the list are filled in correctly
    current = head;
    assert_string_equal(current->name, "SERIAL_NUMBER");
    assert_int_equal(current->flags, FLAG_STRING);
    assert_int_equal(current->length, strlen("PRPL1234567890") + 1);
    assert_memory_equal(current->data, "PRPL1234567890", current->length);

    current = current->next;
    assert_string_equal(current->name, "BASE_MAC_ADDRESS");
    assert_int_equal(current->flags, FLAG_ARRAY); // Assuming current->flags is FLAG_ARRAY for array type
    assert_int_equal(current->length, 6);         // Assuming the length of the array is fixed at 6 bytes
    uint8_t expected_mac_address[] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xAB};
    assert_memory_equal(current->data, expected_mac_address, current->length);

    current = current->next;
    // Assertions for MANUFACTURER entry
    assert_string_equal(current->name, "MANUFACTURER");
    assert_int_equal(current->flags, FLAG_STRING);        // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("WNC") + 1); // Length includes null terminator
    assert_string_equal(current->data, "WNC");

    current = current->next;
    // Assertions for MANUFACTURER_OUI entry
    assert_string_equal(current->name, "MANUFACTURER_OUI");
    assert_int_equal(current->flags, FLAG_ARRAY); // Assuming current->flags is FLAG_ARRAY for array type
    assert_int_equal(current->length, 3);         // Assuming the length of the array is fixed at 3 bytes
    uint8_t expected_oui[] = {0xAC, 0x91, 0x9B};
    assert_memory_equal(current->data, expected_oui, current->length);

    current = current->next;
    // Assertions for MANUFACTURER_URL entry
    assert_string_equal(current->name, "MANUFACTURER_URL");
    assert_int_equal(current->flags, FLAG_STRING);                              // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("https://www.wnc.com.tw/en") + 1); // Length includes null terminator
    assert_string_equal(current->data, "https://www.wnc.com.tw/en");

    current = current->next;
    // Assertions for MODEL_NAME entry
    assert_string_equal(current->name, "MODEL_NAME");
    assert_int_equal(current->flags, FLAG_STRING);                    // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("prpl_gateway_01") + 1); // Length includes null terminator
    assert_string_equal(current->data, "prpl_gateway_01");

    current = current->next;
    // Assertions for PRODUCT_CLASS entry
    assert_string_equal(current->name, "PRODUCT_CLASS");
    assert_int_equal(current->flags, FLAG_STRING);            // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("CR1000A") + 1); // Length includes null terminator
    assert_string_equal(current->data, "CR1000A");

    current = current->next;
    // Assertions for HARDWARE_VERSION entry
    assert_string_equal(current->name, "HARDWARE_VERSION");
    assert_int_equal(current->flags, FLAG_STRING);                     // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("PRPLDEVICE_0_0_1") + 1); // Length includes null terminator
    assert_string_equal(current->data, "PRPLDEVICE_0_0_1");

    current = current->next;
    // Assertions for WLAN_PASSPHRASE entry
    assert_string_equal(current->name, "WLAN_PASSPHRASE");
    assert_int_equal(current->flags, FLAG_STRING);                       // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("PRPLWiFiPassPhrase") + 1); // Length includes null terminator
    assert_string_equal(current->data, "PRPLWiFiPassPhrase");

    current = current->next;
    // Assertions for WLAN_SSID entry
    assert_string_equal(current->name, "WLAN_SSID");
    assert_int_equal(current->flags, FLAG_STRING);                 // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("PRPLWiFiSSID") + 1); // Length includes null terminator
    assert_string_equal(current->data, "PRPLWiFiSSID");

    current = current->next;
    // Assertions for ADMIN_PWD entry
    assert_string_equal(current->name, "ADMIN_PWD");
    assert_int_equal(current->flags, FLAG_STRING);                      // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("PRPLAdminPassword") + 1); // Length includes null terminator
    assert_string_equal(current->data, "PRPLAdminPassword");

    current = current->next;
    // Assertions for DEVICE_CERT entry
    assert_string_equal(current->name, "DEVICE_CERT");
    assert_int_equal(current->flags, FLAG_STRING);                 // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("PRPLX509CERT") + 1); // Length includes null terminator
    assert_string_equal(current->data, "PRPLX509CERT");

    current = current->next;
    // Assertions for DEVICE_CERT_PRIVATE entry
    assert_string_equal(current->name, "DEVICE_CERT_PRIVATE");
    assert_int_equal(current->flags, FLAG_STRING);                   // Assuming current->flags is FLAG_STRING for string type
    assert_int_equal(current->length, strlen("PRPLPRIVATEKEY") + 1); // Length includes null terminator
    assert_string_equal(current->data, "PRPLPRIVATEKEY");

    current = current->next;
    assert_null(current);
}

void test_mfg_init_free(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;
    mock_set_fcntl_success(true);

    mfg_init(&head);
    check_values_mfgdtb(head);

    mfg_init(&head);
    check_values_mfgdtb(head);

    mfg_free(&head);
    assert_null(head);
}

void test_mfg_read(UNUSED void** state) {
    struct mfg_entry* head = NULL;
    int ret = 0;
    char* requested_field = "SERIAL_NUMBER";
    unsigned char* buffer = NULL;
    unsigned long length = 0;

    ret = mfg_read(NULL, &head, (void**) &buffer, &length);
    assert_int_equal(ret, MFG_ERR_PERM);

    ret = mfg_read(requested_field, NULL, (void**) &buffer, &length);
    assert_int_equal(ret, MFG_ERR_PERM);

    ret = mfg_read(requested_field, &head, NULL, &length);
    assert_int_equal(ret, MFG_ERR_PERM);

    ret = mfg_read(requested_field, &head, (void**) &buffer, NULL);
    assert_int_equal(ret, MFG_ERR_PERM);

    // Perform a read without initializing first
    ret = mfg_read(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, 0);
    assert_non_null(buffer);
    assert_int_equal(length, 15);
    assert_string_equal(buffer, "PRPL1234567890");
    check_values_mfgdtb(head);

    free(buffer);
    buffer = NULL;
    length = 0;
    mfg_free(&head);

    mfg_init(&head);
    check_values_mfgdtb(head);

    // Perform a read with initializing first, read a string
    ret = mfg_read(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, 0);
    assert_non_null(buffer);
    assert_int_equal(length, 15);
    assert_string_equal(buffer, "PRPL1234567890");

    // Read a raw value
    requested_field = "BASE_MAC_ADDRESS";
    ret = mfg_read(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, 0);
    assert_non_null(buffer);
    assert_int_equal(length, 6);
    uint8_t expected_mac_address[] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xAB};
    assert_memory_equal(buffer, expected_mac_address, length);

    // Test what happens when the malloc doesn't work
    mock_set_malloc_success(false);
    requested_field = "BASE_MAC_ADDRESS";
    ret = mfg_read(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, errno);
    assert_null(buffer);
    assert_int_equal(length, 0);
    mock_set_malloc_success(true);

    // Test return value NULL of mfg_entries_getMfgEntryByName
    mock_set_mfg_entries_getMfgEntryByName_success(false);
    requested_field = "BASE_MAC_ADDRESS";
    ret = mfg_read(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, MFG_ERR_NOENT);
    assert_null(buffer);
    assert_int_equal(length, 0);
    mock_set_mfg_entries_getMfgEntryByName_success(true);

    free(buffer);
    buffer = NULL;
    mfg_free(&head);
    assert_null(head);
}

void test_mfg_read_ciphertext(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;
    int ret = 0;
    char* requested_field = "SERIAL_NUMBER";
    unsigned char* buffer = NULL;
    unsigned long length = 0;

    ret = mfg_read_ciphertext(NULL, &head, (void**) &buffer, &length);
    assert_int_equal(ret, MFG_ERR_PERM);

    ret = mfg_read_ciphertext(requested_field, NULL, (void**) &buffer, &length);
    assert_int_equal(ret, MFG_ERR_PERM);

    ret = mfg_read_ciphertext(requested_field, &head, NULL, &length);
    assert_int_equal(ret, MFG_ERR_PERM);

    ret = mfg_read_ciphertext(requested_field, &head, (void**) &buffer, NULL);
    assert_int_equal(ret, MFG_ERR_PERM);

    // Perform a read without initializing first
    ret = mfg_read_ciphertext(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, 0);
    assert_non_null(buffer);
    assert_int_equal(length, 15);
    assert_string_equal(buffer, "PRPL1234567890");

    free(buffer);
    buffer = NULL;
    mfg_free(&head);

    mfg_init(&head);
    check_values_mfgdtb(head);

    // Perform a read with initializing first, read a string
    ret = mfg_read_ciphertext(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, 0);
    assert_non_null(buffer);
    assert_int_equal(length, 15);
    assert_string_equal(buffer, "PRPL1234567890");

    // Read a raw value
    requested_field = "BASE_MAC_ADDRESS";
    ret = mfg_read_ciphertext(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, 0);
    assert_non_null(buffer);
    assert_int_equal(length, 6);
    uint8_t expected_mac_address[] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xAB};
    assert_memory_equal(buffer, expected_mac_address, length);

    // Test what happens when the malloc doesn't work
    mock_set_malloc_success(false);
    requested_field = "BASE_MAC_ADDRESS";
    ret = mfg_read(requested_field, &head, (void**) &buffer, &length);
    assert_int_equal(ret, errno);
    assert_null(buffer);
    assert_int_equal(length, 0);
    mock_set_malloc_success(true);


    free(buffer);
    mfg_free(&head);
    assert_null(head);
}

void test_mfg_getMfgEntryNames(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;
    int count = 0;

    mfg_init(&head);
    check_values_mfgdtb(head);

    char** names = mfg_getMfgEntryNames(head, &count);
    assert_non_null(names);
    assert_int_equal(count, 13);

    // Check the returned names list
    assert_string_equal(names[0], "SERIAL_NUMBER");
    assert_string_equal(names[1], "BASE_MAC_ADDRESS");
    assert_string_equal(names[2], "MANUFACTURER");
    assert_string_equal(names[3], "MANUFACTURER_OUI");
    assert_string_equal(names[4], "MANUFACTURER_URL");
    assert_string_equal(names[5], "MODEL_NAME");
    assert_string_equal(names[6], "PRODUCT_CLASS");
    assert_string_equal(names[7], "HARDWARE_VERSION");
    assert_string_equal(names[8], "WLAN_PASSPHRASE");
    assert_string_equal(names[9], "WLAN_SSID");
    assert_string_equal(names[10], "ADMIN_PWD");
    assert_string_equal(names[11], "DEVICE_CERT");
    assert_string_equal(names[12], "DEVICE_CERT_PRIVATE");

    free(names);
    mfg_free(&head);
}

void test_parse_dtb(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;
    void* fdt = NULL;

    // Test return value on failing fdt_check_header in parse_dtb
    mock_set_fdt_check_header_success(false);
    // Create a fdt that is not NULL, but not valid either
    char* formalloc = "AMalloc";
    fdt = malloc(strlen(formalloc));
    parse_dtb(fdt, &head);
    assert_null(head);
    free(fdt);
    fdt = NULL;
    // Test the fdt_check_header of the function fdt_mfgsize()
    mfg_init(&head);
    assert_null(head);
    mock_set_fdt_check_header_success(true);

    // Test return value on failing fdt_subnode_offset
    mock_set_fdt_subnode_offset_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_fdt_subnode_offset_success(true);

    // Test return value on failing fdt_subnode_offset
    mock_set_fdt_first_subnode_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_fdt_first_subnode_success(true);

    // Test return value on failing calloc
    mock_set_calloc_success(false);
    will_return(__wrap_calloc, 1);
    mfg_init(&head);
    assert_null(head);
    mock_set_calloc_success(true);

    // Test return value on failing second calloc
    mock_set_calloc_success(false);
    will_return_always(__wrap_calloc, 2);
    mfg_init(&head);
    assert_null(head);
    mock_set_calloc_success(true);

    // Test return value on failing fdt_get_name
    mock_set_fdt_get_name_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_fdt_get_name_success(true);

    // Test return value on failing strncpy. Is not possible like this
    mock_set_strncpy_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_strncpy_success(true);

    // Test return value on failing fdt_getprop
    mock_set_fdt_getprop_success(false);
    for(int i = 0; i < 13; i++) {
        will_return(__wrap_fdt_getprop, 1);
    }
    mfg_init(&head);
    assert_null(head);
    mock_set_fdt_getprop_success(true);

    // Test return value on failing fdt_getprop
    mock_set_fdt_getprop_success(false);
    // The first call is each time the getting the type, this should succeed.
    // The next call is the value, that is what should fail here.
    for(int i = 0; i < 26; i++) {
        will_return(__wrap_fdt_getprop, 2);
        //will_return(__wrap_fdt_getprop, 1);
    }

    mfg_init(&head);
    assert_null(head);
    mock_set_fdt_getprop_success(true);

    // Check the second failing malloc
    mock_set_malloc_success(false);
    mock_set_malloc_first_fail(false);
    will_return(__wrap_malloc, 2);
    will_return(__wrap_malloc, 2);
    mfg_init(&head);
    assert_null(head);
    mock_set_malloc_first_fail(true);
    mock_set_malloc_success(true);

    // Parameter head is NULL
    // Not possible to test functions that are static. Still looking for the best solution
    parse_dtb(fdt, &head);

    // Test MFG list already contains node with the same name
    mock_set_mfg_entries_containsMfgEntryName_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_mfg_entries_containsMfgEntryName_success(true);

    // Test strcmp not working
    mock_set_strncmp_success(false);
    mfg_init(&head);
    //assert_null(head);
    mock_set_strncmp_success(true);

    mock_set_parse_dtb_success(false);
    mfg_init(&head);
    //assert_null(head);
    mock_set_parse_dtb_success(true);

    // Other error checks from this function can't be tested this way.
    // For that other mocks will have to be created.

    mfg_free(&head);
}

void test_read_dtb(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    // Check when the malloc fails
    mock_set_malloc_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_malloc_success(true);

    // Test return value on failing open
    mock_set_open_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_open_success(true);

    // Test return value on failing read
    mock_set_read_success(false);
    will_return(__wrap_read, 1);
    mfg_init(&head);
    assert_null(head);
    mock_set_read_success(true);

    // Test return value on failing fdt32_to_cpu
//    mock_set_fdt32_to_cpu_success(false);
//    mfg_init(&head);
//    assert_null(head);
//    mock_set_fdt32_to_cpu_success(true);

    // Test return value on failing lseek
    mock_set_lseek_success(false);
    mfg_init(&head);
    assert_null(head);
    mock_set_lseek_success(true);

    // Test return value on setting wrong offset lseek
    mock_set_lseek_correct_offset(false);
    mfg_init(&head);
    //assert_null(head);
    mock_set_lseek_correct_offset(true);

    // Test return value on failing read while going over the buffer
    mock_set_read_success(false);
    will_return(__wrap_read, 2);
    will_return(__wrap_read, 2);
    mfg_init(&head);
    //assert_null(head);
    mock_set_read_success(true);

    // Test on failing fcntl
    mock_set_fcntl_success(false);
    int result = mfg_init(&head);
    assert_int_equal(result, MFG_ERR_IO);
    assert_null(head);
    mock_set_fcntl_success(true);

    mfg_free(&head);
}

void test_mfg_write(UNUSED void** state) {
    // This test only tests the function when MFG_RW_FILE is not enabled.
    char* field = "";
    struct mfg_entry* head = NULL;
    char* type = "someType";
    void* data = (void*) 12345;
    unsigned long length = 10;

    int result = mfg_write(field, &head, type, data, length);
    assert_int_equal(result, MFG_ERR_PERM);
}
