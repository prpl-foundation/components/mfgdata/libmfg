/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mfg_api.h"
#include "mfg_entries.h"
#include "test_mfg_entries.h"
#include "malloc_mock.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

// Function to initialize a single mfg_entry instance
static struct mfg_entry* initialize_mfg_entry(const char* name, const void* data, uint32_t length, uint32_t flags) {
    // Allocate memory for the mfg_entry instance
    struct mfg_entry* entry = (struct mfg_entry*) malloc(sizeof(struct mfg_entry));
    if(entry == NULL) {
        // Handle memory allocation failure
        return NULL;
    }

    // Allocate memory for the name and copy it
    entry->name = strdup(name);
    if(entry->name == NULL) {
        // Handle memory allocation failure
        free(entry);
        return NULL;
    }

    // Allocate memory for the data and copy it
    entry->data = malloc(length);
    if(entry->data == NULL) {
        // Handle memory allocation failure
        free(entry->name);
        free(entry);
        return NULL;
    }
    memcpy(entry->data, data, length);

    // Set the length, flags, and next pointer
    entry->length = length;
    entry->flags = flags;
    entry->next = NULL;

    // Set the secure values
    entry->secure = 0;
    entry->is_encrypted = 0;
    entry->is_signed = 0;
    entry->cipherkey_name = NULL;

    // Return the initialized entry
    return entry;
}

// Function to free memory allocated for an mfg_entry instance
static void free_mfg_entry(struct mfg_entry* entry) {
    if(entry == NULL) {
        // If the entry pointer is NULL, there's nothing to free
        return;
    }

    // Free the name if it was allocated
    if(entry->name != NULL) {
        free(entry->name);
    }

    // Free the data if it was allocated
    if(entry->data != NULL) {
        free(entry->data);
    }

    // Free the entry itself
    free(entry);
}

void test_mfg_entries_append(UNUSED void** state) {
    struct mfg_entry* entry1 = initialize_mfg_entry("Entry1Name", "Entry1Data", strlen("Entry1Data"), FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry("Entry2Name", "Entry2Data", strlen("Entry2Data"), FLAG_STRING);
    struct mfg_entry* entry3 = initialize_mfg_entry("Entry3Name", "Entry3Data", strlen("Entry3Data"), FLAG_STRING);
    assert_non_null(entry1);
    assert_non_null(entry2);
    assert_non_null(entry3);

    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    // Call the function under test
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    // Ensure that head->next is NULL after the first append operation
    assert_null(head->next);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    // Pass NULL pointer for entry
    mfg_entries_append(&head, NULL);

    // Clean up the memory
    free_mfg_entry(entry1);
    free_mfg_entry(entry2);
    free_mfg_entry(entry3);
}

void test_mfg_entries_replace(UNUSED void** state) {
    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Data";
    char* entry1_replacement_data = "Entry1DataReplacement";
    char* entry2_replacement_data = "Entry2DataReplacement";
    struct mfg_entry* entry1 = initialize_mfg_entry("Entry1Name", entry1_data, strlen("Entry1Data"), FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry("Entry2Name", entry2_data, strlen("Entry2Data"), FLAG_STRING);
    struct mfg_entry* entry3 = initialize_mfg_entry("Entry3Name", "Entry3Data", strlen("Entry3Data"), FLAG_STRING);
    struct mfg_entry* entry1_replacement = initialize_mfg_entry("Entry1Name", entry1_replacement_data, strlen("Entry1DataReplacement"), FLAG_STRING);
    struct mfg_entry* entry2_replacement = initialize_mfg_entry("Entry2Name", entry2_replacement_data, strlen("Entry2DataReplacement"), FLAG_STRING);

    assert_non_null(entry1);
    assert_non_null(entry2);
    assert_non_null(entry3);
    assert_non_null(entry1_replacement);
    assert_non_null(entry2_replacement);

    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    // Create a linked list with the entries
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);
    assert_int_equal(head->length, strlen(entry1_data));

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);
    assert_int_equal(head->next->length, strlen(entry2_data));

    // When adding a node where the name is not the same as one already in the list, it should just append it.
    mfg_entries_replace(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    // Change the value/data of entry 1 and replace the old entry1 in the list
    mfg_entries_replace(&head, entry1_replacement);
    assert_ptr_equal(head, entry1_replacement);
    assert_int_equal(head->length, strlen(entry1_replacement_data)); // Check that the data actually changed.

    // Test to replace a value further in the list
    mfg_entries_replace(&head, entry2_replacement);
    assert_ptr_equal(head->next, entry2_replacement);
    assert_int_equal(head->next->length, strlen(entry2_replacement_data)); // Check that the data actually changed.

    // Pass NULL pointer for head
    mfg_entries_replace(NULL, entry1);

    // Pass NULL pointer for entry
    mfg_entries_replace(&head, NULL);

    // Clean up the memory
    free_mfg_entry(entry3);
    free_mfg_entry(entry1_replacement);
    free_mfg_entry(entry2_replacement);
}

void test_mfg_entries_getMfgEntryByName(UNUSED void** state) {
    char* entry1_name = "Entry1Name";
    char* entry2_name = "Entry2Name";
    char* entry3_name = "Entry3Name";

    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Data";
    char* entry3_data = "Entry3Data";

    int entry1_datalen = strlen(entry1_data) + 1;
    int entry2_datalen = strlen(entry2_data) + 1;
    int entry3_datalen = strlen(entry3_data) + 1;

    struct mfg_entry* entry1 = initialize_mfg_entry(entry1_name, entry1_data, entry1_datalen, FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry(entry2_name, entry2_data, entry2_datalen, FLAG_STRING);
    struct mfg_entry* entry3 = initialize_mfg_entry(entry3_name, entry3_data, entry3_datalen, FLAG_STRING);

    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    // Call the function under test
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    // Ensure that head->next is NULL after the first append operation
    assert_null(head->next);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    // Retrieve the first entry by the name and check if it is in the list correctly
    struct mfg_entry* returnedEntry = mfg_entries_getMfgEntryByName(head, entry1_name);
    assert_string_equal(returnedEntry->name, entry1_name);
    assert_string_equal(returnedEntry->data, entry1_data);
    assert_int_equal(returnedEntry->length, entry1_datalen);
    assert_int_equal(returnedEntry->flags, FLAG_STRING);

    // Retrieve the second entry by the name and check if it is in the list correctly
    returnedEntry = mfg_entries_getMfgEntryByName(head, entry2_name);
    assert_string_equal(returnedEntry->name, entry2_name);
    assert_string_equal(returnedEntry->data, entry2_data);
    assert_int_equal(returnedEntry->length, entry2_datalen);
    assert_int_equal(returnedEntry->flags, FLAG_STRING);

    // Retrieve the third entry by the name and check if it is in the list correctly
    returnedEntry = mfg_entries_getMfgEntryByName(head, entry3_name);
    assert_string_equal(returnedEntry->name, entry3_name);
    assert_string_equal(returnedEntry->data, entry3_data);
    assert_int_equal(returnedEntry->length, entry3_datalen);
    assert_int_equal(returnedEntry->flags, FLAG_STRING);

    // Check to retrieve a name that is not in the list
    returnedEntry = mfg_entries_getMfgEntryByName(head, "NotInTheList");
    assert_null(returnedEntry);

    // Pass head NULL
    returnedEntry = mfg_entries_getMfgEntryByName(NULL, "HEADNULLTEST");
    assert_null(returnedEntry);

    // Pass name NULL
    returnedEntry = mfg_entries_getMfgEntryByName(head, NULL);
    assert_null(returnedEntry);

    // Test when the entry name is NULL
    free(entry3->name);
    entry3->name = NULL;
    returnedEntry = mfg_entries_getMfgEntryByName(head, entry3_name);
    assert_null(returnedEntry);

    // Clean up the memory
    free_mfg_entry(entry1);
    free_mfg_entry(entry2);
    free_mfg_entry(entry3);
}

void test_mfg_entries_containsMfgEntryName(UNUSED void** state) {
    // Create a small list
    char* entry1_name = "Entry1Name";
    char* entry2_name = "Entry2Name";
    char* entry3_name = "Entry3Name";

    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Data";
    char* entry3_data = "Entry3Data";

    int entry1_datalen = strlen(entry1_data) + 1;
    int entry2_datalen = strlen(entry2_data) + 1;
    int entry3_datalen = strlen(entry3_data) + 1;

    struct mfg_entry* entry1 = initialize_mfg_entry(entry1_name, entry1_data, entry1_datalen, FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry(entry2_name, entry2_data, entry2_datalen, FLAG_STRING);
    struct mfg_entry* entry3 = initialize_mfg_entry(entry3_name, entry3_data, entry3_datalen, FLAG_STRING);

    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    // Call the function under test
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    // Ensure that head->next is NULL after the first append operation
    assert_null(head->next);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    // Check that the list contains the node entry1 up to entry3
    int result_contains_name = mfg_entries_containsMfgEntryName(head, entry1_name);
    assert_int_equal(result_contains_name, 1);

    result_contains_name = mfg_entries_containsMfgEntryName(head, entry2_name);
    assert_int_equal(result_contains_name, 1);

    result_contains_name = mfg_entries_containsMfgEntryName(head, entry3_name);
    assert_int_equal(result_contains_name, 1);

    // Check it will return 0 when a name is not found
    result_contains_name = mfg_entries_containsMfgEntryName(head, "NotARealName");
    assert_int_equal(result_contains_name, 0);

    // Clean up the memory
    free_mfg_entry(entry1);
    free_mfg_entry(entry2);
    free_mfg_entry(entry3);
}

void test_mfg_entries_getMfgEntryNames(UNUSED void** state) {
    // Create a small list
    char* entry1_name = "Entry1Name";
    char* entry2_name = "Entry2Name";
    char* entry3_name = "Entry3Name";

    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Data";
    char* entry3_data = "Entry3Data";

    int entry1_datalen = strlen(entry1_data) + 1;
    int entry2_datalen = strlen(entry2_data) + 1;
    int entry3_datalen = strlen(entry3_data) + 1;

    struct mfg_entry* entry1 = initialize_mfg_entry(entry1_name, entry1_data, entry1_datalen, FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry(entry2_name, entry2_data, entry2_datalen, FLAG_STRING);
    struct mfg_entry* entry3 = initialize_mfg_entry(entry3_name, entry3_data, entry3_datalen, FLAG_STRING);

    // Initialize the head pointer
    struct mfg_entry* head = NULL;
    int count = 0;

    // Test that the function does not return a list now
    char** returnedNamesList = mfg_entries_getMfgEntryNames(head, &count);
    assert_null(returnedNamesList);
    assert_int_equal(count, 0);

    // Call the function under test
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    // Ensure that head->next is NULL after the first append operation
    assert_null(head->next);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    returnedNamesList = mfg_entries_getMfgEntryNames(head, &count);
    assert_non_null(returnedNamesList);
    assert_int_equal(count, 3);

    // Check the returned names list
    assert_string_equal(returnedNamesList[0], entry1_name);
    assert_string_equal(returnedNamesList[1], entry2_name);
    assert_string_equal(returnedNamesList[2], entry3_name);

    // Test what happens when malloc fails
    mock_set_malloc_success(false);
    char** returnedNullList = mfg_entries_getMfgEntryNames(head, &count);
    assert_null(returnedNullList);

    mock_set_malloc_success(true);

    // Clean up the memory
    free(returnedNamesList);
    free_mfg_entry(entry1);
    free_mfg_entry(entry2);
    free_mfg_entry(entry3);
}

void test_mfg_entries_displayList(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    mfg_entries_displayList(head);

    // Create a small list
    char* entry1_name = "Entry1Name";
    char* entry2_name = "Entry2Name";
    char* entry3_name = "Entry3Name";

    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Data";
    char* entry3_data = "Entry3Data";

    int entry1_datalen = strlen(entry1_data) + 1;
    int entry2_datalen = strlen(entry2_data) + 1;
    int entry3_datalen = strlen(entry3_data) + 1;

    struct mfg_entry* entry1 = initialize_mfg_entry(entry1_name, entry1_data, entry1_datalen, FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry(entry2_name, entry2_data, entry2_datalen, FLAG_STRING);
    struct mfg_entry* entry3 = initialize_mfg_entry(entry3_name, entry3_data, entry3_datalen, FLAG_STRING);

    // Test that the function does not return a list now
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    // Ensure that head->next is NULL after the first append operation
    assert_null(head->next);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    // Print the list:
    mfg_entries_displayList(head);

    free_mfg_entry(entry1);
    free_mfg_entry(entry2);
    free_mfg_entry(entry3);
}

void test_mfg_entries_getCipheredLength(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    int returnLength = mfg_entries_getCipheredLength(head);
    assert_int_equal(returnLength, 0);

    // Create a small list
    char* entry1_name = "Entry1Name";
    char* entry2_name = "Entry2Name";
    char* entry3_name = "Entry3Name";

    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Data";
    char* entry3_data = "Entry3Data";

    int entry1_datalen = strlen(entry1_data) + 1;
    int entry2_datalen = strlen(entry2_data) + 1;
    int entry3_datalen = strlen(entry3_data) + 1;

    struct mfg_entry* entry1 = initialize_mfg_entry(entry1_name, entry1_data, entry1_datalen, FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry(entry2_name, entry2_data, entry2_datalen, FLAG_ARRAY);
    struct mfg_entry* entry3 = initialize_mfg_entry(entry3_name, entry3_data, entry3_datalen, FLAG_ARRAY);

    // Test that the function does not return a list now
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    returnLength = mfg_entries_getCipheredLength(head);
    assert_int_equal(returnLength, 0);

    head->secure = 1;
    returnLength = mfg_entries_getCipheredLength(head);
    assert_int_equal(returnLength, (entry1_datalen - 1));

    head->next->is_signed = 1;
    returnLength = mfg_entries_getCipheredLength(head);
    assert_int_equal(returnLength, ((entry1_datalen - 1) + entry2_datalen));

    free_mfg_entry(entry1);
    free_mfg_entry(entry2);
    free_mfg_entry(entry3);
}

void test_mfg_entries_fillCipheredBuffer(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;
    char* smfgdata = NULL;
    char* smfgdata_signed = NULL;

    int resultFillingBuffer = mfg_entries_fillCipheredBuffer(head, &smfgdata);
    assert_int_equal(resultFillingBuffer, -1);

    // Create a small list
    char* entry1_name = "Entry1Name";
    char* entry2_name = "Entry2Name";
    char* entry3_name = "Entry3Name";

    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Dataaaaaaaa";
    char* entry3_data = "Entry3Data";

    int entry1_datalen = strlen(entry1_data) + 1;
    int entry2_datalen = strlen(entry2_data) + 1;
    int entry3_datalen = strlen(entry3_data) + 1;

    struct mfg_entry* entry1 = initialize_mfg_entry(entry1_name, entry1_data, entry1_datalen, FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry(entry2_name, entry2_data, entry2_datalen, FLAG_ARRAY);
    struct mfg_entry* entry3 = initialize_mfg_entry(entry3_name, entry3_data, entry3_datalen, FLAG_ARRAY);

    // Test that the function does not return a list now
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    // Make entry1 secure
    head->secure = 1;

    // Create the smfgData buffer
    int smfgdata_len = mfg_entries_getCipheredLength(head);
    assert_int_equal(smfgdata_len, (entry1_datalen - 1));

    smfgdata = malloc(smfgdata_len);

    resultFillingBuffer = mfg_entries_fillCipheredBuffer(head, &smfgdata);
    assert_int_equal(resultFillingBuffer, 0);

    // Test is_signed on entry2
    head->secure = 0;
    head->next->is_signed = 1;

    smfgdata_len = mfg_entries_getCipheredLength(head);
    assert_int_equal(smfgdata_len, (entry2_datalen));
    smfgdata_signed = malloc(smfgdata_len);

    resultFillingBuffer = mfg_entries_fillCipheredBuffer(head, &smfgdata_signed);
    assert_int_equal(resultFillingBuffer, 0);

    free(smfgdata);
    free(smfgdata_signed);
    free_mfg_entry(entry1);
    free_mfg_entry(entry2);
    free_mfg_entry(entry3);
}

void test_mfg_entries_freeList(UNUSED void** state) {
    // Initialize the head pointer
    struct mfg_entry* head = NULL;

    // Create a small list
    char* entry1_name = "Entry1Name";
    char* entry2_name = "Entry2Name";
    char* entry3_name = "Entry3Name";

    char* entry1_data = "Entry1Data";
    char* entry2_data = "Entry2Dataaaaaaaa";
    char* entry3_data = "Entry3Data";

    int entry1_datalen = strlen(entry1_data) + 1;
    int entry2_datalen = strlen(entry2_data) + 1;
    int entry3_datalen = strlen(entry3_data) + 1;

    struct mfg_entry* entry1 = initialize_mfg_entry(entry1_name, entry1_data, entry1_datalen, FLAG_STRING);
    struct mfg_entry* entry2 = initialize_mfg_entry(entry2_name, entry2_data, entry2_datalen, FLAG_ARRAY);
    struct mfg_entry* entry3 = initialize_mfg_entry(entry3_name, entry3_data, entry3_datalen, FLAG_ARRAY);

    // Test that the function does not return a list now
    mfg_entries_append(&head, entry1);
    assert_ptr_equal(head, entry1);

    mfg_entries_append(&head, entry2);
    assert_ptr_equal(head->next, entry2);

    mfg_entries_append(&head, entry3);
    assert_ptr_equal(head->next->next, entry3);

    // Free the list
    mfg_entries_freeList(&head);
}
