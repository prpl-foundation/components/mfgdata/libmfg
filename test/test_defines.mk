MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../../../include ../../include_priv)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(SRCDIR)/mfg.c $(SRCDIR)/mfg_entries.c

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) \

CFLAGS += -DUNIT_TEST
CFLAGS += -DLIB_MFG_RO_PART_FILE=\"$(shell pwd)/mfg.dtb\"

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lsahtrace -l:libfdt.a -lfdt

LDFLAGS += -g
