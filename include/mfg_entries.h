/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef MFG_ENTRIES_H
#define MFG_ENTRIES_H

#include <stdint.h>
#include "mfg_api.h"

/**
 * @defgroup mfg_entries MFG Entries
 *
 * @brief
 * This module defines functions to create and control the linked list
 * of mfg_entry nodes.
 */

/**
 * @ingroup mfg_entries
 * @brief
 * Function to insert a new node at the end of the list
 *
 * @param head Pointer to the head (first node) of the linked list.
 * @param newmfg_entry The new node (mfg_entry) that needs to be added to the list.
 *
 * @returns none
 */
void mfg_entries_append(struct mfg_entry** head, struct mfg_entry* newmfg_entry);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to replace an entry with same name if found, or add it at the end otherwise
 * If a previous entry is replaced by the new one it will be freed
 *
 * @param head Pointer to the head (first node) of the linked list.
 * @param newmfg_entry The new node (mfg_entry) that needs to be added to the list.
 *
 * @returns none
 */
void mfg_entries_replace(struct mfg_entry** head, struct mfg_entry* newmfg_entry);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to check if the linked list already contains a node with the name
 *
 * @param head Pointer to the head (first node) of the linked list.
 * @param name The name of the node (mfg_entry) checked if present in the list.
 *
 * @returns 1 when it contains the name
 */
int mfg_entries_containsMfgEntryName(struct mfg_entry* head, const char* name);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to get the mfg_entry from the list by the name passed as parameter.
 *
 * @param head Pointer to the head (first node) of the linked list.
 * @param name The name of the node (mfg_entry) that needs to be returned.
 *
 * @returns mfg_entry* when successful, NULL when the name is not in the list
 */
struct mfg_entry* mfg_entries_getMfgEntryByName(struct mfg_entry* head, const char* name);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to return all the node names in MfgEntryNames
 *
 * @param head Pointer to the head (first node) of the linked list.
 * @param count Pointer to the number of names present in the linked list (the number of nodes)
 *
 * @returns char** containing the names of the linked list
 */
char** mfg_entries_getMfgEntryNames(struct mfg_entry* head, int* count);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to display the linked list
 *
 * @param head Pointer to the head (first node) of the linked list.
 *
 * @returns none
 */
void mfg_entries_displayList(struct mfg_entry* head);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to check which node is ciphered in the list and return the total length.
 *
 * @param head Pointer to the head (first node) of the linked list.
 *
 * @returns Total length of ciphered fields.
 */
int mfg_entries_getCipheredLength(struct mfg_entry* head);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to fill the buffer smfgdata with the ciphered data from the list
 *
 * @param head Pointer to the head (first node) of the linked list.
 * @param smfgdata Buffer where the signed data will be stored.
 *
 * @returns 0 on success, -1 on failure
 */
int mfg_entries_fillCipheredBuffer(struct mfg_entry* head, char** smfgdata);

/**
 * @ingroup mfg_entries
 * @brief
 * Function to free the memory allocated for the linked list
 *
 * @param head Pointer to the head (first node) of the linked list.
 *
 * @returns none
 */
void mfg_entries_freeList(struct mfg_entry** head);

#endif /* MFG_ENTRIES_H */
