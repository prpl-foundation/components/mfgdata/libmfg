/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef MFG_KEYRING_H
#define MFG_KEYRING_H

#include <stdint.h>
#include <stddef.h>

#define AES_BLOCK_SIZE 16  // AES block size for IV

/**
 * @defgroup mfg_keyring MFG Keyring
 *
 * @brief
 * This module defines functions to decrypt a ciphered field with a key from
 * the kernel keyring. This is done with the help of kcapi and keyutils
 */

/**
 * @ingroup mfg_keyring
 * @brief Reads the initialization vector (IV) from a file.
 *
 * This function reads an IV from a specified file and stores it in the provided buffer.
 * The IV should be in hexadecimal format, with each byte represented by two hexadecimal digits.
 * If the file cannot be opened or contains invalid data, the function returns -1.
 *
 * @param filename the name of the file to read the IV from
 * @param iv a pointer to the buffer to store the IV in
 * @param iv_len the length of the IV buffer
 * @return int 0 on success, -1 on failure
 */
int read_iv_from_file(const char* filename, unsigned char* iv, size_t iv_len);

/**
 * @ingroup mfg_keyring
 * @brief Decrypts AES-CBC data using a specified key from the kernel keyring.
 *
 * This function takes an encrypted buffer of AES-CBC data along with the initialization vector (IV),
 * and the name of the key from the kernel keyring to use for decryption. It allocates memory for the decrypted data,
 * which is then returned by the function along with its length in bytes. If the decryption
 * fails for any reason, the function returns -1.
 *
 * @param encrypted_data a pointer to the encrypted AES-CBC data buffer
 * @param encrypted_size the size of the encrypted data buffer
 * @param iv a pointer to the initialization vector (IV) used for encryption
 * @param key_name the name of the key to use for decryption
 * @param data a pointer to a pointer to store the decrypted data in
 * @param length a pointer to a size_t variable to store the length of the decrypted data
 * @return int 0 on success, -1 on failure
 */
int decrypt_aes_cbc(const unsigned char* encrypted_data, size_t encrypted_size, const unsigned char* iv, const char* key_name, unsigned char** data, size_t* length);

#endif /* MFG_KEYRING_H */