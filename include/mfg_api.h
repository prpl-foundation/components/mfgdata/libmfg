/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef LIBMFG_H
#define LIBMFG_H

#include <stdint.h>

#define FLAG_STRING             (1 << 0)
#define FLAG_ARRAY              (1 << 1)
#define FLAG_MFG_RW             (1 << 2)

/**
 * @defgroup mfg_api MFG API
 *
 * @brief
 * This module defines functions and data structures to interface with lib_mfg.
 * lib_mfg is used for reading the MFG data.
 */

/**
 * @ingroup mfg_api
 * @brief Data structure representing a Manufacturing (MFG) entry/field.
 *
 * This structure defines a single MFG field within the MFG data. It encapsulates
 * information about the field's name, data length, data pointer, flags, and its
 * position in a linked list.
 */
struct mfg_entry {
    char* name;              /**< The name of the MFG field.
                                This is a null-terminated string that describes the field's identifier. */

    uint32_t length;         /**< The length (in bytes) of the data associated with the field.
                                This value indicates the size of the data stored in the 'data' pointer. */

    void* data;              /**< A pointer to the actual data associated with the MFG field.
                                The data can be of any type, and its interpretation depends on the field. */

    uint32_t flags;          /**< Flags or attributes associated with the MFG field.
                                These flags provide additional information or metadata about the field. */

    struct mfg_entry* next;  /**< Pointer to the next MFG entry in the linked list.
                                This pointer is used to navigate through the linked list of MFG entries. */

    uint8_t secure;          /**< Indicates whether the MFG field is secured.
                                If 'secure' is non-zero, the field is considered secure; otherwise, it is not. */

    uint8_t is_encrypted;    /**< Indicates whether the MFG field is encrypted/ciphered.
                                If 'is_encrypted' is non-zero, the field is encrypted; otherwise, it is not. */

    uint8_t is_signed;       /**< Indicates whether the MFG field is signed.
                                If 'is_signed' is non-zero, the field is digitally signed; otherwise, it is not. */

    char* cipherkey_name     /**< The name of the cipher key used for encryption (if 'is_encrypted' is non-zero).
                                This is a null-terminated string that specifies the encryption key's identifier. */;
};

/**
 * @ingroup mfg_api
 * @brief
 * Initialize the internal structures of the Manufacturing data
 * if necessary.
 * The MFG data is first read and than parsed.
 * The values retrieved from the MFG are stored in a linked list
 * of mfg_entry's. The head of the list should be given by the caller
 * and the memory should also be freed by the caller.
 *
 * @param head  Pointer to where the head of the linked list should be stored.
 *              Memory should be freed by the caller.
 *
 * @return 0 on success. On error < 0.
 */
int mfg_init(struct mfg_entry** head);

/**
 * @ingroup mfg_api
 * @brief
 * Acquire the value (data) and the length from the requested field
 * The head of the linked list needs to be passed as well. If there is no linked list
 * created yet, the function will initialize it.
 *
 * @param field Name of the field that should be read (ex: SERIAL_NUMBER)
 * @param head Pointer to where the head of the linked list should be stored.
 *             Memory should be freed by the caller.
 * @param data Pointer on an allocated buffer where the data is stored.
 *             The memory should be freed by the caller.
 * @param length Length of the data pointed by data.
 *
 * @return MFG_OK on success
 * On Error: MFG_ERR_PERM, MFG_ERR_NOENT, MFG_ERR_IO, MFG_ERR_NOSPC
 */
int mfg_read(char* field, struct mfg_entry** head, void** data, unsigned long* length);

/**
 * @ingroup mfg_api
 * @brief Return an undecrypted value from the MFG.
 *
 * If the field is encrypted, it will not be decrypted.
 *
 * Acquire the value (data) and the length from the requested field
 * The head of the linked list needs to be passed as well. If there is no linked list
 * created yet, the function will initialize it.
 *
 * @param field Name of the field that should be read (ex: SERIAL_NUMBER)
 * @param head Pointer to where the head of the linked list should be stored.
 *             Memory should be freed by the caller.
 * @param data Pointer on an allocated buffer where the data is stored.
 *             The memory should be freed by the caller.
 * @param length Length of the data pointed by data.
 *
 * @return MFG_OK on success
 * On Error: MFG_ERR_PERM, MFG_ERR_NOENT, MFG_ERR_IO, MFG_ERR_NOSPC
 */
int mfg_read_ciphertext(char* field, struct mfg_entry** head, void** data, unsigned long* length);

/**
 * @ingroup mfg_api
 * @brief Write a raw value in the RW MFG.
 *
 * @param field Name of the field that should be written (ex: SERIAL_NUMBER).
 * @param head Pointer to where the head of the linked list should be stored.
 *             List will contain updated value for written field in case of success only.
 *             Memory should be freed by the caller.
 * @param type Type of the MFG entry (raw/string).
 * @param data Pointer on an allocated buffer where the data is stored.
 *             The memory should be freed by the caller.
 * @param length Length of the data pointed by data.
 *
 * @return MFG_OK on success
 * On Error: MFG_ERR_PERM, MFG_ERR_NOENT, MFG_ERR_IO, MFG_ERR_NOSPC
 */
int mfg_write(char* field, struct mfg_entry** head, char* type, void* data, unsigned long length);

/**
 * @ingroup mfg_api
 * @brief
 * Function to free the complete linked list containing the MFG data
 *
 * @param head Pointer to where the head of the linked list should be stored.
 *
 * @return none
 */
void mfg_free(struct mfg_entry** head);

/**
 * @ingroup mfg_api
 * @brief Function to retrieve the names of all the MFG entries that are present in the linked list.
 *
 * @param head Pointer to where the head of the linked list should be stored. Memory should be freed by the caller.
 * @param count Pointer to the number of names present in the linked list (the number of nodes).
 *
 * @return char** containing the names of the linked list.
 */
char** mfg_getMfgEntryNames(struct mfg_entry* head, int* count);

#endif  // LIBMFG_H
