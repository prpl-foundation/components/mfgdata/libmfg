/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __MFG_DEFS_H__
#define __MFG_DEFS_H__

/**
 * Return values for mfg_read().
 */
#define MFG_OK                0    /* Success. */
#define MFG_ERR_PERM         -1    /* Operation not permitted: this field cannot be written. */
#define MFG_ERR_NOENT        -2    /* Invalid field number. */
#define MFG_ERR_IO           -5    /* I/O error while writing. */
#define MFG_FDT_ERR_BADMAGIC -9    /* Given "device tree" appears not to be a device tree at all - */
                                   /* it is missing the flattened device tree magic  */
#define MFG_ERR_BAD_HEX      -10   /* The hex string value is not as expected */
#define MFG_ERR_KEYRING      -11   /* There is an error with decrypting a filed with a kernel keyring key */
#define MFG_ERR_NOSPC        -28   /* Data too large. */

#define MFG_FDT_NODE_NAME              "MFG_DATA"
#define MFG_FDT_PROP_VALUE             "value"         /* Value of the field */
#define MFG_FDT_PROP_TYPE              "type"          /* type of the field (string, raw,...) */
#define MFG_FDT_PROP_SECURE            "secure"        /* means both encrypted and signed (deprecated) */
#define MFG_FDT_PROP_IS_ENCRYPTED      "is_encrypted"  /* field is encrypted (deprecated) */
#define MFG_FDT_PROP_IS_SIGNED         "is_signed"     /* field is part of the signature (deprecated)*/

#define MFG_FDT_CIPHER_NODE_NAME       "cipher"        /* Sub-node containing the cipher information */
#define MFG_FDT_PROP_CIPHER_KEY_NAME   "key_name"      /* Property containing the key name */
#define MFG_FDT_PROP_CIPHER_FORMAT     "format"        /* Property containing the format of the ciphered data */

#define MFG_FDT_SIG_NODE_NAME          "signature"     /* Sub-node containing the signature information */
#define MFG_FDT_PROP_SIG_INDEX         "index"         /* Index of the RIP field containing the signature */

#endif /* __MFG_DEPS_H__ */