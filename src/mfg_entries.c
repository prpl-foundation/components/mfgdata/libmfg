/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mfg_entries.h"
#include "mfg_api.h"
#include "mfg_logs.h"


// Function to insert a new mfg_entry at the end of the list
void mfg_entries_append(struct mfg_entry** head, struct mfg_entry* newmfg_entry) {
    if(newmfg_entry == NULL) {

        ERROR("The received newmfg_entry is a NULL pointer");
        return;
    }

    if(*head == NULL) {
        *head = newmfg_entry;
    } else {
        struct mfg_entry* current = *head;
        while(current->next != NULL) {
            current = current->next;
        }
        current->next = newmfg_entry;
    }
}

// Function to replace an entry with same name if found, or add it at the end otherwise
void mfg_entries_replace(struct mfg_entry** head, struct mfg_entry* newmfg_entry) {
    if((head == NULL) || (newmfg_entry == NULL)) {
        ERROR("Received a NULL pointer");
        return;
    }

    for(struct mfg_entry* entry = *head; entry != NULL; head = &entry->next, entry = *head) {
        if(!strcmp(entry->name, newmfg_entry->name)) {
            newmfg_entry->next = entry->next;
            *head = newmfg_entry;
            entry->next = NULL;
            mfg_entries_freeList(&entry);
            return;
        }
    }
    mfg_entries_append(head, newmfg_entry);
}

// Function to check if the linked list contains a certain node
int mfg_entries_containsMfgEntryName(struct mfg_entry* head, const char* name) {
    return mfg_entries_getMfgEntryByName(head, name) != NULL;
}

// Function to return the data and length for a node with a given name
struct mfg_entry* mfg_entries_getMfgEntryByName(struct mfg_entry* head, const char* name) {
    struct mfg_entry* current = NULL;

    if((head == NULL) || (name == NULL)) {
        return NULL;
    }
    current = head;

    while(current != NULL) {
        if((current->name == NULL) || (name == NULL)) {
            current = current->next;
            continue;
        }
        if(!strcmp(current->name, name)) {
            return current;
        }
        current = current->next;
    }
    return NULL;
}

// Function to return all the node names in MfgEntryNames
char** mfg_entries_getMfgEntryNames(struct mfg_entry* head, int* count) {
    if(head == NULL) {
        *count = 0;
        return NULL;
    }

    // Count the number of nodes in the linked list
    int numNodes = 0;
    struct mfg_entry* current = head;
    while(current != NULL) {
        numNodes++;
        current = current->next;
    }

    // Allocate the memory for MfgEntryNames
    char** names = (char**) malloc(numNodes * sizeof(char*));
    if(names == NULL) {
        ERROR("Memory allocation failed in mfg_entries_getMfgEntryNames!\n");
        return NULL;
    }

    // Copy the names in the array
    current = head;
    int i = 0;
    while(current != NULL) {
        names[i] = current->name;
        current = current->next;
        i++;
    }

    *count = numNodes;
    return names;
}

// Function to display the linked list
void mfg_entries_displayList(struct mfg_entry* head) {
    struct mfg_entry* current = head;
    if(current == NULL) {
        ERROR("List is empty.\n");
    } else {
        INFO("List elements:\n");
        while(current != NULL) {
            INFO("Name: %s\n", current->name);
            INFO("Length: %u\n", current->length);
            INFO("Flags: %u\n", current->flags);
            INFO("\n");
            current = current->next;
        }
    }
}


// Function to check which node is ciphered in the list and return the total length
int mfg_entries_getCipheredLength(struct mfg_entry* head) {
    int smfgdata_len = 0;
    struct mfg_entry* current = head;

    while(current != NULL) {
        if(current->secure || current->is_signed) {
            if(current->flags & FLAG_STRING) {
                smfgdata_len += current->length - 1; /* Do not include the NULL terminating character */
            } else {
                smfgdata_len += current->length;
            }
        }
        current = current->next;
    }

    return smfgdata_len;
}


// Function to fill the buffer smfgdata with the ciphered data from the list
// Returns 0 on success
int mfg_entries_fillCipheredBuffer(struct mfg_entry* head, char** smfgdata) {
    struct mfg_entry* current = head;
    char* tmp = *smfgdata;
    int length = 0;
    if((current == NULL) || (tmp == NULL)) {
        return -1;
    }

    while(current != NULL) {
        if(current->secure || current->is_signed) {
            if(current->flags & FLAG_STRING) {
                length += current->length - 1; /* Do not include the NULL terminating character */
            } else {
                length += current->length;
            }
            memcpy(tmp, current->data, length);
            tmp += length;
        }
        current = current->next;
    }

    return 0;
}


// Function to free the memory allocated for the linked list
void mfg_entries_freeList(struct mfg_entry** head) {
    struct mfg_entry* current = *head;
    struct mfg_entry* next = NULL;
    while(current != NULL) {
        next = current->next;
        free(current->name);
        free(current->data);
#ifdef MFG_SECURE_SUPPORT
        free(current->cipherkey_name);
#endif
        free(current);
        current = next;
    }
    *head = NULL;
}
