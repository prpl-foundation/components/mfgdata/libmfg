/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <endian.h>
#include <assert.h>
#include <libfdt_env.h>
#include <libfdt.h>
#include <fdt.h>
#include "mfg_defs.h"
#include "mfg_entries.h"
#include "mfg_logs.h"

#if defined(MFG_SECURE_SUPPORT) || defined(MFG_GLOBAL_SIGNATURE)
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#endif

#if defined(MFG_SECURE_SUPPORT)
#include <mfg_decrypt.h>
#include <sys/ioctl.h>
#endif

#if defined(MFG_SECURE_MFG_KERNEL_KEYRING_SUPPORT)
#include "mfg_keyring.h"
#endif

#if defined(MFG_GLOBAL_SIGNATURE)
#include "globalsig.h"
#endif

#define MFG_RO_FILE LIB_MFG_RO_PART_FILE

#ifdef LIB_MFG_RW_PART_FILE
#define MFG_RW_FILE LIB_MFG_RW_PART_FILE
#define MFG_RW_FILE_TMP MFG_RW_FILE "_tmp"
#endif

#ifdef MFG_RO_PART_UBI
#define MFG_UBI_DEVICE MFG_RO_PART_UBI_DEVICE
#define MFG_UBI_VOLUME MFG_RO_PART_UBI_VOLUME
#endif

#define GLOBAL_SIG_PUBKEY       LIB_MFG_GLOBAL_SIGNATURE_PUBLIC_PEM_FILE

#define ARRAY_SIZE(x)           sizeof(x) / sizeof(x[0])

#define FDT_OVERHEAD            256

#ifdef MFG_SECURE_SUPPORT
static unsigned short sig_verif = 0;
static unsigned char default_cipherkey_name[] = "key-aes256-kuk-ivuk";
#endif

#define KERNEL_KEYRING_KEY_NAME     LIB_MFG_SECURE_MFG_KERNEL_KEYRING_KEY_NAME
#define KERNEL_KEYRING_IV_FILE      LIB_MFG_SECURE_MFG_KERNEL_KEYRING_IV_FILE

/**
 * \brief Parse all nodes of a MFG DTB and add them to the list if not already present.
 *
 * May be called multiple times for multiple MFG DTB (RO/RW).
 * Set FLAG_MFG_RW bit of mfg_flags defined in API header when parsing RW MFG.
 *
 * \param fdt Pointer to the DTB.
 * \param mfg_flags gives flags e.g. if DTB to parse is RW MFG.
 * \param head Pointer to the list head.
 *
 * \return 0 on success or an error code otherwise.
 */
int parse_dtb(void* fdt, int mfg_flags, struct mfg_entry** head) {
    int ret = 0;
    int mfg_data_offset = 0, node_offset = 0;
    const void* node_prop = NULL;
    const void* node_name = NULL;
    int node_len = 0, len = 0;

    if((head == NULL) || (fdt == NULL)) {
        ERROR("parse_dtb: The given head or fdt is NULL\n");
        ret = MFG_ERR_IO;
        goto end;
    }

    if((ret = fdt_check_header(fdt))) {
        ERROR("Corrupted MFG device tree %d", ret);
        ret = MFG_ERR_IO;
        goto end;
    }

    mfg_data_offset = fdt_subnode_offset(fdt, 0, MFG_FDT_NODE_NAME);
    if(mfg_data_offset < 0) {
        ERROR("MFG_DATA not found in device tree %d", ret);
        ret = MFG_ERR_NOENT;
        goto end;
    }
    INFO("MFG_DATA node found in DTB at 0x%x", mfg_data_offset);

    if((node_offset = fdt_first_subnode(fdt, mfg_data_offset)) < 0) {
        ERROR("No subnode found in MFG_DATA node");
        ret = MFG_ERR_NOENT;
        goto end;
    }

    // Create the linked list with all the nodes from MFG_DATA
    do {
#if defined(MFG_SECURE_SUPPORT) || defined(MFG_SECURE_MFG_KERNEL_KEYRING_SUPPORT)
        int subnode_offset;
#endif
        struct mfg_entry* newmfg_entry = calloc(1, sizeof(struct mfg_entry));
        if(newmfg_entry == NULL) {
            ERROR("Could not allocate memory for the newmfg_entry\n");
            goto end;
        }

        node_name = fdt_get_name(fdt, node_offset, &node_len);
        if((NULL == node_name) || (node_len <= 0)) {
            ERROR("Could not retrieve the name from node at offset 0x%x (%d)",
                  node_offset, node_len);
            free(newmfg_entry);
            continue;
        }
        INFO("Found sub-node: %s", (const char*) node_name);

        // Check if the name already exists in the list (double declaration in the MFG_DATA)
        if(mfg_entries_containsMfgEntryName(*head, (const char*) node_name)) {
            ERROR("The MFG data already contains a field with this name: %s. There is a double declaration in the MFG_DATA\n", (const char*) node_name);
            ERROR("Skipping this node, the previous node will be kept\n");
            free(newmfg_entry);
            continue;
        }

        newmfg_entry->name = calloc(1, strlen(node_name) + 1);
        if(newmfg_entry->name == NULL) {
            ERROR("Could not allocate memory for the name of the node");
            free(newmfg_entry);
            goto end;
        }

        strncpy(newmfg_entry->name, node_name, node_len);

        INFO("Copied Found sub-node: %s", newmfg_entry->name);
        if(strlen(newmfg_entry->name) == 0) {
            ERROR("Could not retreive the name from node");
            free(newmfg_entry->name);
            free(newmfg_entry);
            continue;
        }

#if defined(MFG_SECURE_SUPPORT) || defined(MFG_SECURE_MFG_KERNEL_KEYRING_SUPPORT)
        /* If data pointer is freed, cipherkey_name should also be */
        assert(NULL == newmfg_entry->cipherkey_name);

        node_prop = fdt_getprop(fdt, node_offset, MFG_FDT_PROP_SECURE, NULL);
        if(node_prop) {
            newmfg_entry->secure = !!be32toh(*((int32_t*) node_prop));
        } else {
            newmfg_entry->secure = 0;
        }
        INFO("\tSecure: %d", newmfg_entry->secure);

        newmfg_entry->is_encrypted = 0;
        newmfg_entry->is_signed = 0;
        node_prop = fdt_getprop(fdt, node_offset, MFG_FDT_PROP_IS_ENCRYPTED, NULL);
        if(node_prop) {
            newmfg_entry->is_encrypted = !!be32toh(*((int32_t*) node_prop));
        }

        node_prop = fdt_getprop(fdt, node_offset, MFG_FDT_PROP_IS_SIGNED, NULL);
        if(node_prop) {
            newmfg_entry->is_signed = !!be32toh(*((int32_t*) node_prop));
        }
        /* Check if subnodes exist */
        if((subnode_offset = fdt_first_subnode(fdt, node_offset)) >= 0) {
            int subnode_len = 0;
            const void* subnode_name;

            do {
                subnode_name = fdt_get_name(fdt, subnode_offset, &node_len);
                if((NULL == subnode_name) || (subnode_len < 0)) {
                    ERROR("Could not retrieve the name from subnode at offset 0x%x (%d)",
                          subnode_offset, subnode_len);
                    continue;
                }
                if(!strcmp(subnode_name, MFG_FDT_CIPHER_NODE_NAME)) {
                    newmfg_entry->is_encrypted = 1;
                    node_prop = fdt_getprop(fdt, subnode_offset, MFG_FDT_PROP_CIPHER_KEY_NAME, &len);
                    if(node_prop && (len > 0)) {
                        if(NULL == (newmfg_entry->cipherkey_name = calloc(1, len))) {
                            ERROR("malloc failed: %m");
                            /* We probably run out of memory => stop parsing */
                            ret = errno;
                            goto end;
                        }
                        memcpy(newmfg_entry->cipherkey_name, node_prop, len);
                    }
                    //TODO: Retrieve and support the "format" property
                } else if(!strcmp(subnode_name, MFG_FDT_SIG_NODE_NAME)) {
                    newmfg_entry->is_signed = 1;
                    //TODO: Retrieve and support the "index" property
                } else {
                    WARNING("Unknown subnode %s in node %s at offset 0x%x",
                            (const char*) subnode_name, (const char*) node_name, subnode_offset);
                    continue;
                }
            } while (-FDT_ERR_NOTFOUND != (subnode_offset = fdt_next_subnode(fdt, subnode_offset)));
        }
        INFO("\tis_encrypted: %d", newmfg_entry->is_encrypted);
        INFO("\tis_signed: %d", newmfg_entry->is_signed);
#endif

        node_prop = fdt_getprop(fdt, node_offset, MFG_FDT_PROP_TYPE, &len);

        if((NULL == node_prop) || (len < 0)) {
            ERROR("Could not retrieve type property from node %s", (const char*) node_name);
            free(newmfg_entry->name);
            free(newmfg_entry);
            continue;
        }
        INFO("\tType: %s", (const char*) node_prop);

        if(!strncmp(node_prop, "string", len)) {
            newmfg_entry->flags = FLAG_STRING;
        } else if(!strncmp(node_prop, "raw", len)) {
            newmfg_entry->flags = FLAG_ARRAY;
        } else {
            ERROR("Unsupported type property: %s", (const char*) node_prop);
            free(newmfg_entry->name);
            free(newmfg_entry);
            continue;
        }

        if(mfg_flags & FLAG_MFG_RW) {
            newmfg_entry->flags |= FLAG_MFG_RW;
        }

        node_prop = fdt_getprop(fdt, node_offset, MFG_FDT_PROP_VALUE, &len);
        if((NULL == node_prop) || (len < 0)) {
            ERROR("Could not retrieve value property from node %s", (const char*) node_name);
            free(newmfg_entry->name);
            free(newmfg_entry);
            continue;
        }
        if(NULL == (newmfg_entry->data = malloc(len))) {
            ERROR("malloc failed: %m");
            /* We probably ran out of memory => stop parsing */
            free(newmfg_entry->name);
            free(newmfg_entry);
            ret = errno;
            goto end;
        }
        memcpy(newmfg_entry->data, node_prop, len);
        newmfg_entry->length = (uint32_t) len;

#ifdef DEBUG
        if(newmfg_entry->flags & FLAG_STRING) {
            INFO("\tnewmfg_entry->data=%.*s", len, (char*) newmfg_entry->data);
        } else {
            INFO("\tnewmfg_entry->data=%p", newmfg_entry->data);
        }
#endif
        INFO("\tnewmfg_entry->length=%d", len);

        // Add newmfg_entry to the linked list
        mfg_entries_append(head, newmfg_entry);
#ifdef DEBUG
        INFO("Printing the list until now: \n");
        mfg_entries_displayList(*head);
#endif

    } while (-FDT_ERR_NOTFOUND != (node_offset = fdt_next_subnode(fdt, node_offset)));

end:
    return ret;
}


/**
 * Parse the MFG, checking the signature header if necessary
 */
static int parse_mfg(void* mfg, struct mfg_entry** head, size_t mfg_size) {
#ifdef MFG_GLOBAL_SIGNATURE
    int ret = MFG_OK;
    FILE* fp = NULL;
    EVP_PKEY* pubkey = NULL;
    EVP_PKEY_CTX* pkey_ctx = NULL;
    EVP_MD_CTX* md_ctx = NULL;
    int result = 0;
    int pubkey_size = 0;
    unsigned char digest[SHA256_DIGEST_LENGTH];
    struct sig_header_s* sig_hdr = mfg;

    if(be32toh(sig_hdr->magic) != SIG_HEADER_MAGIC) {
        ERROR("MFG does not contain global signature magic");
        ret = MFG_ERR_IO;
        goto end;
    }

    /* First verify the signature of the header itself */
    if(be32toh(sig_hdr->header_size) + be32toh(sig_hdr->image_size) > mfg_size) {
        ERROR("Size in global signature header larger than read data");
        ret = MFG_ERR_IO;
        goto end;
    }
    fp = fopen(GLOBAL_SIG_PUBKEY, "r");
    if(fp == NULL) {
        ERROR("Could not open %s to read the global signature public key", GLOBAL_SIG_PUBKEY);
        ret = MFG_ERR_IO;
        goto end;
    }
    pubkey = PEM_read_PUBKEY(fp, NULL, NULL, NULL);
    if(pubkey == NULL) {
        ERROR("Could not parse public key in file %s", GLOBAL_SIG_PUBKEY);
        ret = MFG_ERR_IO;
        goto end;
    }
    pubkey_size = EVP_PKEY_size(pubkey);
    if(pubkey_size <= 0) {
        ERROR("Could not get the size of the public key. The returned size is: %d", pubkey_size);
        ret = MFG_ERR_IO;
        goto end;
    }

    md_ctx = EVP_MD_CTX_create();
    if(md_ctx == NULL) {
        ERROR("Could not initialise digest context");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(EVP_DigestVerifyInit(md_ctx, &pkey_ctx, EVP_sha256(), NULL, pubkey) != 1) {
        ERROR("Could not initialise verification context");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(EVP_PKEY_CTX_ctrl_str(pkey_ctx, "rsa_padding_mode",
                             LIB_MFG_GLOBAL_SIGNATURE_PADDING_MODE) != 1) {
        ERROR("Failed to set padding mode");
        ret = MFG_ERR_IO;
        goto end;
    }

    if(EVP_DigestVerifyUpdate(md_ctx, sig_hdr, sizeof(struct sig_header_s)) != 1) {
        ERROR("Failed to hash signature header");
        ret = MFG_ERR_IO;
        goto end;
    }
    uint8_t* sig = (uint8_t*) sig_hdr + sizeof(struct sig_header_s);
    result = EVP_DigestVerifyFinal(md_ctx, sig, pubkey_size);
    if(result < 0) {
        ERROR("Failed to finalise verification digest");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(result != 1) {
        ERROR("Signature verification for header signature failed, the error code is: %d", result);
        ret = MFG_ERR_IO;
        goto end;
    }
    EVP_MD_CTX_reset(md_ctx);

    /* Now check the sha256 hash of the image signature, contained in that header */
    if(EVP_DigestInit_ex(md_ctx, EVP_sha256(), NULL) != 1) {
        ERROR("Could not initialise hash context");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(EVP_DigestUpdate(md_ctx, mfg + be32toh(sig_hdr->offset_img_sig), pubkey_size) != 1) {
        ERROR("Could not hash MFG");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(EVP_DigestFinal_ex(md_ctx, digest, NULL) != 1) {
        ERROR("Failed to get MFG hash");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(memcmp(digest, &sig_hdr->sha256_img_sig, SHA256_DIGEST_LENGTH) != 0) {
        ERROR("MFG hash does not match expected value!");
        ret = MFG_ERR_IO;
        goto end;
    }
    EVP_MD_CTX_reset(md_ctx);

    /* Finally verify the actual signature of the dtb */
    if(EVP_DigestVerifyInit(md_ctx, &pkey_ctx, EVP_sha256(), NULL, pubkey) != 1) {
        ERROR("Could not initialise verification context");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(EVP_PKEY_CTX_ctrl_str(pkey_ctx, "rsa_padding_mode",
                             LIB_MFG_GLOBAL_SIGNATURE_PADDING_MODE) != 1) {
        ERROR("Failed to set padding mode");
        ret = MFG_ERR_IO;
        goto end;
    }
    char* mfg_data = mfg + be32toh(sig_hdr->header_size);
    if(EVP_DigestVerifyUpdate(md_ctx, mfg_data, be32toh(sig_hdr->image_size)) != 1) {
        ERROR("Failed to hash MFG DTB");
        ret = MFG_ERR_IO;
        goto end;
    }
    const unsigned char* image_sig = mfg + be32toh(sig_hdr->offset_img_sig);
    result = EVP_DigestVerifyFinal(md_ctx, image_sig, pubkey_size);
    if(result < 0) {
        ERROR("Failed to finalise verification digest");
        ret = MFG_ERR_IO;
        goto end;
    }
    if(result != 1) {
        ERROR("Signature verification for MFG DTB failed");
        ret = MFG_ERR_IO;
        goto end;
    }
    mfg = mfg_data;

end:
    if(pubkey != NULL) {
        EVP_PKEY_free(pubkey);
    }
    if(md_ctx != NULL) {
        EVP_MD_CTX_destroy(md_ctx); /* frees pkey_ctx too */
    }
    if(fp != NULL) {
        fclose(fp);
    }

    return ret == MFG_OK ? parse_dtb(mfg, 0, head) : ret;

#else /* MFG_GLOBAL_SIGNATURE */
    (void) mfg_size;
    return parse_dtb(mfg, 0, head);
#endif /* MFG_GLOBAL_SIGNATURE */
}

/**
 * \brief Open RO or RW MFG for reading and set read lock.
 *
 * \param fd Pointer to store the file descriptor if return is MFG_OK.
 * \param dev_path Path to the file.
 *
 * \return 0 on success or an error code otherwise. MFG_ERR_NOENT is a valid case for uninitialized RW MFG.
 */
static int open_mfg(int* fd, const char* dev_path) {
    struct flock fl = { .l_type = F_RDLCK,
        .l_whence = SEEK_SET,
        .l_start = 0,
        .l_len = 0 };

    *fd = open(dev_path, O_RDONLY);
    if(*fd < 0) {
        return MFG_ERR_NOENT;
    }

    /* Read lock on the MFG file is taken before reading it */
    if(fcntl(*fd, F_SETLKW, &fl) < 0) {
        ERROR("Failed locking %s in shared mode: %m", dev_path);
        close(*fd);
        *fd = -1;
        return MFG_ERR_IO;
    }

    return MFG_OK;
}

#ifdef MFG_GLOBAL_SIGNATURE
/**
 * \brief Read global signature header and get total expected size.
 *
 * \param fd File to check the header and get the expected size from.
 * \param mfg_size Pointer to store the fetched size (if return is MFG_OK).
 *
 * \return 0 on success or an error code otherwise.
 */
static int sig_hdr_mfgsize(int fd, uint32_t* mfg_size) {
    struct sig_header_s sig_hdr;
    ssize_t byte_read = read(fd, &sig_hdr, sizeof(struct sig_header_s));

    if(byte_read != sizeof(struct sig_header_s)) {
        ERROR("Failed to read global signature header");
        return byte_read < 0 ? errno : MFG_ERR_IO;
    }
    if(be32toh(sig_hdr.magic) != SIG_HEADER_MAGIC) {
        ERROR("MFG does not contain global signature magic");
        return MFG_ERR_IO;
    }

    *mfg_size = be32toh(sig_hdr.header_size) + be32toh(sig_hdr.image_size);
    return MFG_OK;
}
#endif

#if !defined (MFG_GLOBAL_SIGNATURE) || defined (MFG_RW_FILE)
/**
 * \brief Read FDT header and get total expected size.
 *
 * \param fd File to check the header and get the expected size from.
 * \param mfg_size Pointer to store the fetched size (if return is MFG_OK).
 *
 * \return 0 on success or an error code otherwise.
 */
static int fdt_mfgsize(int fd, uint32_t* mfg_size) {
    struct fdt_header fdtheader;
    ssize_t byte_read = read(fd, &fdtheader, sizeof(struct fdt_header));

    if(byte_read != sizeof(struct fdt_header)) {
        ERROR("Failed to read FDT header");
        return byte_read < 0 ? errno : MFG_ERR_IO;
    }

    if(fdt_check_header(&fdtheader) != 0) {
        ERROR("Bad FDT header");
        return MFG_FDT_ERR_BADMAGIC;
    }

    *mfg_size = fdt_totalsize(&fdtheader);
    return MFG_OK;
}
#endif

/**
 * \brief Allocate a buffer to load given file. Optionally allocate extra space and resize DTB.
 *
 * We need to load both RO and RW MFG.
 * For RW MFG we need to allocate extra_space to add node, in which case DTB needs to be resized.
 * Never set extra_space to non-zero value for RO MFG which may have a global signature header, resizing would corrupt it.
 *
 * \param fd File to load.
 * \param bufp Pointer to where the allocated buffer should be stored.
 * \param mfg_size Size of the data to load.
 * \param extra_space Additional size for the buffer and to resize the DTB.
 *
 * \return 0 on success or an error code otherwise.
 */
static int load_mfg(int fd, uint8_t** bufp, uint32_t mfg_size, uint32_t extra_space) {
    int ret = MFG_OK;
    uint8_t* p = NULL;
    int r = -1;
    uint8_t* buf;

    if(bufp == NULL) {
        ERROR("load_mfg: invalid NULL pointer given");
        return MFG_ERR_PERM;
    }

    buf = malloc(mfg_size + extra_space);
    if(buf == NULL) {
        ERROR("malloc failed to allocate %u bytes: %m", mfg_size + extra_space);
        return errno;
    }

    if(lseek(fd, 0L, SEEK_SET) < 0) {
        ERROR("lseek failed: %m");
        ret = errno;
        goto err;
    }

    for(p = buf; (p - buf) < (unsigned int) mfg_size && (r = read(fd, p, mfg_size - (p - buf))); p += r) {
        if(r < 0) {
            ERROR("read failed: %m");
            ret = errno;
            goto err;
        }
    }

    if(p - buf < (unsigned int) mfg_size) {
        unsigned long diff = p - buf;
        ERROR("Failed to read entire MFG, got %lu out of %u bytes", diff, mfg_size);
        ret = MFG_ERR_IO;
        goto err;
    }

    if(extra_space != 0) {
        fdt_set_totalsize(buf, mfg_size + extra_space);
    }

    *bufp = buf;
    return ret;
err:
    free(buf);
    return ret;
}

/**
 * \brief Read and parse a DTB from a device.
 *
 * This function can be used for UBI, eMMC, v-flash, and a file
 * The resulting data are stored in a static array for later use.
 * This function should not be called directly and is not static in order to
 * be used by the unit tests.
 *
 * \param dev_path The device path (e.g. "/dev/ubi0_65" for UBI, or "/dev/factoryparams" for eMMC) containing a DTB to be parsed.
 * \param head The first node of the list.
 *
 * \return 0 on success or an error code otherwise.
 */
static int read_dtb(const char* dev_path, struct mfg_entry** head) {
    int ret = MFG_OK, fd = -1;
    uint32_t mfg_size = 0;
    uint8_t* buf = NULL;

    INFO("The path where the mfg data is taken from: %s\n", dev_path);
    ret = open_mfg(&fd, dev_path);
    switch(ret) {
    case MFG_OK:
        break;
    case MFG_ERR_NOENT:
        ERROR("opening %s failed: %m", dev_path);
        return ret;
    default:
        return ret;
    }

#ifdef MFG_GLOBAL_SIGNATURE
    ret = sig_hdr_mfgsize(fd, &mfg_size);
#else
    ret = fdt_mfgsize(fd, &mfg_size);
#endif
    if(ret == MFG_OK) {
        INFO("mfgsize is %u", mfg_size);

        ret = load_mfg(fd, &buf, mfg_size, 0);
        if(ret == MFG_OK) {
            parse_mfg(buf, head, mfg_size);
            free(buf);
        }
    }

    close(fd);
    return ret;
}

void mfg_free(struct mfg_entry** head) {
    mfg_entries_freeList(head);
}


#ifdef MFG_SECURE_SUPPORT
/**
 * \brief Verify the signature of signed fields in MFG
 *
 * \return 0 on success or -1 on error.
 */
static int verify_smfg_signature(struct mfg_entry* head) {
#ifdef SMFG_PUB_PEM_FILE
    int ret = -1;
    int i, cnt, length = 0;
    unsigned long err;
    char* smfgdata = NULL, * sig = NULL, * tmp = NULL;
    int smfgdata_len, sig_len;
    FILE* fp = NULL;
    EVP_MD_CTX* mdctx = NULL;
    EVP_PKEY_CTX* pkctx = NULL;
    EVP_PKEY* pkey = NULL;

    /* Load the crypto library error strings */
    OPENSSL_init_crypto(OPENSSL_INIT_LOAD_CRYPTO_STRINGS, NULL);

    /* Read the public key */
    fp = fopen(SMFG_PUB_PEM_FILE, "r");
    if(NULL == fp) {
        ERROR("Failed opening public key PEM file %s: %m", SMFG_PUB_PEM_FILE);
        goto end;
    }

    /* Get the public key */
    pkey = PEM_read_PUBKEY(fp, NULL, NULL, NULL);
    fclose(fp);
    if(pkey == NULL) {
        err = ERR_get_error();
        ERROR("Failed to retrieve the public key from %s", SMFG_PUB_PEM_FILE);
        ERROR("Failed in %s: %lu - %s", ERR_func_error_string(err), err, ERR_reason_error_string(err));
        goto end;
    }

    /* Create the Message Digest Context */
    if(NULL == (mdctx = EVP_MD_CTX_create())) {
        err = ERR_get_error();
        ERROR("EVP_MD_CTX_create() failed in %s: %lu - %s",
              ERR_func_error_string(err), err, ERR_reason_error_string(err));
        goto end;
    }

    /* Allocate an array where to store the signed data */
    // Loop over the list and check which node is ciphered
    smfgdata_len = mfg_entries_getCipheredLength(head); // TODO: Extend this function that it also passes the count of ciphered fields

    INFO("SMFG buffer: Total length: %d - Number of fields signed: %d", smfgdata_len, cnt);

    if(NULL == (smfgdata = malloc(smfgdata_len))) {
        ERROR("malloc failed: %m");
        goto end;
    }

    // Fill the buffer with all the ciphered data
    if(mfg_entries_fillCipheredBuffer(head, &smfgdata) != 0) {
        ERROR("Failed to fill the buffer with the ciphered data");
        goto end;
    }

    sig = base64_decode(mfg_entry[MFG_PRIV_SECURE_MFG_SIGN].data,
                        mfg_entry[MFG_PRIV_SECURE_MFG_SIGN].length, &sig_len);

    INFO("SMFG signature length: %d", sig_len);

    /* Verify the signature */
    if(1 != EVP_DigestVerifyInit(mdctx, &pkctx, EVP_sha256(), NULL, pkey)) {
        err = ERR_get_error();
        ERROR("Failed in %s: %lu - %s", ERR_func_error_string(err), err, ERR_reason_error_string(err));
        goto end;
    }

    /* Set specific settings for the signature verification.
     * Assume RSA_PKCS1_PSS_PADDING and rsa_pss_saltlen set to -1 (salt length equals digest length) */
    EVP_PKEY_CTX_set_rsa_padding(pkctx, RSA_PKCS1_PSS_PADDING);
    EVP_PKEY_CTX_set_rsa_pss_saltlen(pkctx, -1);

    if((1 != EVP_DigestVerifyUpdate(mdctx, smfgdata, smfgdata_len)) ||
       (1 != EVP_DigestVerifyFinal(mdctx, sig, sig_len))) {
        err = ERR_get_error();
        ERROR("Failed in %s: %lu - %s", ERR_func_error_string(err), err, ERR_reason_error_string(err));
    } else {
        ret = 0;
    }

end:
    free(sig);
    free(smfgdata);
    if(mdctx) {
        EVP_MD_CTX_destroy(mdctx); /* Also frees pkctx */
    }
    if(pkey) {
        EVP_PKEY_free(pkey);
    }

    return ret;
#else
    return 0;
#endif /* SMFG_PUB_PEM_FILE */
}
#endif /* MFG_SECURE_SUPPORT */

#ifdef MFG_RO_PART_UBI
/* Filter function for scandir to find ubi volume id from ubi volume name */
static int filter_ubi_volumes(const struct dirent* entry) {
    return !strncmp(entry->d_name, MFG_UBI_DEVICE "_", sizeof(MFG_UBI_DEVICE));
}
#endif

int mfg_init(struct mfg_entry** head) {
    int ret = MFG_OK;
    if(*head != NULL) { /* DTB has already been parsed */
        mfg_free(head);
        *head = NULL;
    }

#if defined(MFG_RO_PART_UBI)
    INFO("Reading the MFG from ubi volume: %s", MFG_RO_FILE);
    struct dirent** ubi_volumes;

    int entcnt = scandir("/sys/class/ubi", &ubi_volumes, &filter_ubi_volumes, NULL);
    if(entcnt <= 0) {
        ERROR("Could not find any ubi volumes on UBI device %s at path /sys/class/ubi", MFG_UBI_DEVICE);
        return MFG_ERR_IO;
    }

    char volume_name[128] = {0}; /* Maximum UBI volume name length is 127 */
    int n = 0;
    for(n = 0; n < entcnt; n++) {
        char* volume = (char*) &(ubi_volumes[n]->d_name);
        int volume_len = strlen(volume);
        char volume_name_path[sizeof("/sys/class/ubi/" "/name") + volume_len];
        int volume_name_file = -1;
        int volume_name_len = 0;
        sprintf(volume_name_path, "%s%s%s", "/sys/class/ubi/", volume, "/name");
        if((volume_name_file = open(volume_name_path, O_RDONLY)) < 0) {
            ERROR("Could not open volume name to check %s", volume_name_path);
            continue;
        }
        if((volume_name_len = read(volume_name_file, &volume_name, 128)) == 0) {
            ERROR("Could not read volume name file %s", volume_name_path);
            close(volume_name_file);
            continue;
        }
        if(close(volume_name_file) != 0) {
            ERROR("Failed to close volume name file %s", volume_name_path);
            continue;
        }
        volume_name[volume_name_len] = 0;
        if(!strcmp(volume_name, MFG_UBI_VOLUME "\n")) {
            char ubi_volume[sizeof("/dev/") + volume_len];
            sprintf(ubi_volume, "%s%s", "/dev/", volume);
            ret = read_dtb(ubi_volume, head);
            break;
        }
    }
    if(n == entcnt) {
        ERROR("Could not find ubi volume %s", MFG_RO_FILE);
        ret = MFG_ERR_IO;
    }

    for(n = 0; n < entcnt; n++) {
        free(ubi_volumes[n]);
    }
    free(ubi_volumes);

    if(MFG_OK != ret) {
        return ret;
    }

#else
    INFO("Reading the MFG from: %s", MFG_RO_FILE);
    if(MFG_OK != (ret = read_dtb(MFG_RO_FILE, head))) {
        return ret;
    }
#endif /* MFG_RO_PART_UBI */

#ifdef MFG_SECURE_SUPPORT
    if(0 == (sig_verif = !verify_smfg_signature(*head))) {
        WARNING("Signature verification failed! Secured MFG fields will not be accessible!");
    }
#endif

#ifdef MFG_RW_FILE
    // Also load RW MFG
    int fd = -1;
    uint8_t* fdt = NULL;
    uint32_t mfg_size = 0;

    if(open_mfg(&fd, MFG_RW_FILE) == MFG_OK) {
        int ret_rw = MFG_OK;

        if(((ret_rw = fdt_mfgsize(fd, &mfg_size)) != MFG_OK)
           || ((ret_rw = load_mfg(fd, &fdt, mfg_size, 0)) != MFG_OK)) {
            ERROR("Unexpected error loading RW MFG %d", ret_rw);
        } else {
            parse_dtb(fdt, FLAG_MFG_RW, head);
            free(fdt);
        }

        close(fd);
    }
#endif

    return ret;
}

/**
 * \brief Return a requested value from the MFG, optionally decrypting it.
 *
 * \param field The field to retrieve
 * \param head Pointer to the head of the linked list
 *             Memory should be freed by the caller.
 * \param data Pointer to an array which will be set to point on data pointer of the field.
 *             Memory should be freed by the caller.
 * \param length Pointer where the length of the data field is stored.
 * \param ciphertext Return the ciphertext if the field is encrypted.
 *
 * \return 0 on success or an error code otherwise.
 */
static int read_mfg(char* field, struct mfg_entry** head, void** data, unsigned long* length, unsigned int ciphertext) {
    struct mfg_entry* requested_mfg_entry = NULL;

    if((NULL == data) || (NULL == length) || (NULL == head)) {
        ERROR("read_mfg: invalid NULL pointer given");
        return MFG_ERR_PERM;
    }

    if(field == NULL) {
        ERROR("No field provided\n");
        return MFG_ERR_PERM;
    }

    /* Initialize default returned values */
    // Make sure that the buffer is empty/freed before doing the malloc again
    if(*data != NULL) {
        free(*data);
    }
    *data = NULL;
    *length = 0;

    // Check if the linked list already exists
    if(*head == NULL) { // If the head is NULL, no linked list has been created yet
        int ret_init = mfg_init(head);
        if(ret_init != MFG_OK) {
            ERROR("the list is non-existing or the provided field is NULL");
            return ret_init;
        }
    }

    // Get the mfg_entry_data struct which contains the correct name from the linked list
    requested_mfg_entry = mfg_entries_getMfgEntryByName(*head, field);
    if((requested_mfg_entry == NULL) || (requested_mfg_entry->data == NULL) || (requested_mfg_entry->length == 0)) {
        INFO("No valid data for field name: %s\n", field);
        return MFG_ERR_NOENT;
    }
#if defined(MFG_SECURE_SUPPORT)
    if((requested_mfg_entry->secure || requested_mfg_entry->is_encrypted) && !ciphertext) {  /* Decipher the encrypted data */
        // mfg_decrypt_info_t should be included from the kernel module mfg_decrypt!
        mfg_decrypt_info_t info = {0};
        int fd = -1;

        if(!sig_verif) {
            ERROR("Encrypted MFG fields are not accessible");
            return MFG_ERR_PERM;
        }

        info.encrypted_data = requested_mfg_entry->data;
        info.encrypted_datalen = requested_mfg_entry->length;
        info.decrypted_datalen = info.encrypted_datalen;
        if(requested_mfg_entry->cipherkey_name) {
            info.cipherkey_name = (__u8*) requested_mfg_entry->cipherkey_name;
            info.cipherkey_namelen = strlen(requested_mfg_entry->cipherkey_name) + 1;  /* Including the NULL terminating character */
        } else {                                                                       /* Try with the default key name for Kuk */
            info.cipherkey_name = default_cipherkey_name;
            info.cipherkey_namelen = strlen((const char*) default_cipherkey_name) + 1; /* Including the NULL terminating character */
        }
        /* Decrypted data will always be smaller than the encrypted ones */
        if(NULL == (info.decrypted_data = calloc(1, info.decrypted_datalen))) {
            ERROR("calloc failed (%ln bytes): %m", length);
            return errno;
        }

        fd = open("/dev/mfg", O_RDWR);
        if(fd < 0) {
            ERROR("Cannot open device /dev/mfg: %m");
            free(info.decrypted_data);
            return errno;
        }

        //TODO: Check return value from ioctl
        ioctl(fd, MFG_DECRYPT, &info);

        *data = info.decrypted_data;
        *length = info.decrypted_datalen;

        close(fd);
    } else {
#elif defined(MFG_SECURE_MFG_KERNEL_KEYRING_SUPPORT)
    unsigned char iv[AES_BLOCK_SIZE] = {0};

    if(requested_mfg_entry->is_encrypted && !ciphertext) {  /* Decipher the encrypted data */
        // First retrieve the iv key from the file
        if(read_iv_from_file(KERNEL_KEYRING_IV_FILE, iv, AES_BLOCK_SIZE) < 0) {
            ERROR("Error: Failed to load IV from file\n");
            return MFG_ERR_BAD_HEX;
        }

        // decrypt the requested field with the keyring key
        if(decrypt_aes_cbc(requested_mfg_entry->data, requested_mfg_entry->length, iv, KERNEL_KEYRING_KEY_NAME, (unsigned char**) data, length) < 0) {
            ERROR("ERROR: Failed to decrypt the aes cbc value");
            return MFG_ERR_KEYRING;
        }

    } else {
#else
    (void) ciphertext;
    {
#endif /* MFG_SECURE_SUPPORT */
        INFO("Returning %s of size %u - Pointer address %p",
             requested_mfg_entry->name, requested_mfg_entry->length, requested_mfg_entry->data);
        if(NULL == (*data = malloc(requested_mfg_entry->length))) {
            ERROR("malloc failed (%d bytes): %m", requested_mfg_entry->length);
            return errno;
        }
        memcpy(*data, requested_mfg_entry->data, requested_mfg_entry->length);
        *length = requested_mfg_entry->length;
    }
    return MFG_OK;
}

#ifdef MFG_RW_FILE
/**
 * \brief Opens the RW MFG and edit all fields requested.
 *
 * Check if the field we want to write isn't RO, if yes then error.
 * Loads RW MFG or create one from scratch if it doesn't exists or is corrupted.
 * If it exists but we fail to load it due to IO error then return error.
 * If RW MFG is loaded from file delete field to edit if it was already defined.
 * Add field to write to the RW MFG and write to file.
 *
 * \param field Field name.
 * \param head Pointer to the head of the linked list.
 *             Will be initialized will all MFG including new field in case of success.
 * \param type MFG entry type.
 * \param data Pointer to the data to write.
 * \param length Data length.
 *
 * \return 0 on success or an error code otherwise.
 */
static int write_mfg(char* field, struct mfg_entry** head, char* type, void* data, unsigned long length) {
    int ret = MFG_OK, fd = -1, fd_tmp = -1;
    struct mfg_entry* mfg_entry = NULL, * newmfg_entry = NULL;
    uint8_t* fdt = NULL;
    int base_off = -1, node_off = -1;
    uint32_t mfg_size = 0;
    struct flock fl = { .l_type = F_WRLCK,
        .l_whence = SEEK_SET,
        .l_start = 0,
        .l_len = 0 };

    if((field == NULL) || (head == NULL) || (type == NULL) || (data == NULL)) {
        ERROR("write_mfg: invalid NULL pointer given");
        return MFG_ERR_PERM;
    }

    if(!(newmfg_entry = calloc(1, sizeof(struct mfg_entry)))
       || !(newmfg_entry->name = strdup(field))
       || !(newmfg_entry->data = malloc(length))) {
        ERROR("Failed: to allocate memory for new MFG entry: %m");
        mfg_entries_freeList(&newmfg_entry);
        return errno;
    }

    memcpy(newmfg_entry->data, data, length);
    newmfg_entry->length = length;

    if(!strcmp(type, "string")) {
        newmfg_entry->flags = FLAG_STRING | FLAG_MFG_RW;
    } else if(!strcmp(type, "raw")) {
        newmfg_entry->flags = FLAG_ARRAY | FLAG_MFG_RW;
    } else {
        ERROR("Invalid type value '%s'", type);
        ret = MFG_ERR_PERM;
        goto end;
    }

    if((ret = mfg_init(head)) != MFG_OK) {
        ERROR("mfg_init returned error code %d!\n", ret);
        goto end;
    }

    mfg_entry = mfg_entries_getMfgEntryByName(*head, field);
    if((mfg_entry != NULL) && !(mfg_entry->flags & FLAG_MFG_RW)) {
        ERROR("Cannot redefine MFG fields already defined in RO MFG");
        ret = MFG_ERR_PERM;
        goto end;
    }

    if((fd_tmp = open(MFG_RW_FILE_TMP, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR)) < 0) {
        ERROR("Failed to open %s: %m", MFG_RW_FILE_TMP);
        ret = errno;
        goto end;
    }

    /* Write lock on the MFG RW TMP file is taken before writing to it */
    if(fcntl(fd_tmp, F_SETLKW, &fl) < 0) {
        ERROR("Failed locking %s in exclusive mode: %m", MFG_RW_FILE_TMP);
        ret = errno;
        goto end;
    }

    switch(ret = open_mfg(&fd, MFG_RW_FILE)) {
    case MFG_OK:
        if((ret = fdt_mfgsize(fd, &mfg_size)) == MFG_OK) {
            if((ret = load_mfg(fd, &fdt, mfg_size, length + FDT_OVERHEAD)) != MFG_OK) {
                ERROR("Unexpected error loading RW MFG %d", ret);
                goto end;
            }
            if((base_off = fdt_subnode_offset(fdt, 0, "MFG_DATA")) >= 0) {
                if((node_off = fdt_subnode_offset(fdt, base_off, field)) >= 0) {
                    fdt_del_node(fdt, node_off);
                }
                break;
            }
            free(fdt);
        }
        ERROR("MFG_DATA node not found in invalid RW MFG %d", ret);
        // fallthrough if RW MFG is read but invalid and make a new one from scratch
        __attribute__((fallthrough));
    case MFG_ERR_NOENT:
        INFO("No RW MFG currently exists, creating one from scratch");
        if((fdt = malloc(2048)) == NULL) {
            ERROR("malloc failed: %m");
            ret = errno;
            goto end;
        }
        fdt_create_empty_tree(fdt, 2048);
        base_off = fdt_add_subnode(fdt, 0, "MFG_DATA");
        break;
    default:
        ERROR("Unexpected error loading RW MFG %d", ret);
        goto end;
    }

    if((node_off = fdt_add_subnode(fdt, base_off, field)) < 0) {
        ERROR("Failed adding subnode %s to RW MFG device tree: %d",
              field, node_off);
        ret = MFG_ERR_NOSPC;
        goto end;
    }

    if(((ret = fdt_setprop_string(fdt, node_off, "type", type)) != 0)
       || ((ret = fdt_setprop(fdt, node_off, "value", data, length)) != 0)) {
        ERROR("Failed setting properties of node %s", field);
        ret = MFG_ERR_NOSPC;
        goto end;
    }
    fdt_pack(fdt);

    if(fdt_totalsize(fdt) == write(fd_tmp, fdt, fdt_totalsize(fdt))) {
        ret = MFG_OK;
    } else {
        ERROR("Failed writing to %s", MFG_RW_FILE_TMP);
        ret = MFG_ERR_IO;
    }

end:
    free(fdt);
    close(fd);
    /* If we wrote something to the temp file (MFG_OK), copy it to the actual file */
    if(ret == MFG_OK) {
        if(rename(MFG_RW_FILE_TMP, MFG_RW_FILE) < 0) {
            ret = errno;
            ERROR("renaming %s to %s failed: %m", MFG_RW_FILE_TMP, MFG_RW_FILE);
            remove(MFG_RW_FILE_TMP);
        } else {
            mfg_entries_replace(head, newmfg_entry);
            newmfg_entry = NULL;
        }
    }
    mfg_entries_freeList(&newmfg_entry);
    close(fd_tmp); /* This will also remove the lock on the file */
    return ret;
}

#endif

int mfg_read(char* field, struct mfg_entry** head, void** data, unsigned long* length) {
    return read_mfg(field, head, data, length, 0);
}

int mfg_read_ciphertext(char* field, struct mfg_entry** head, void** data, unsigned long* length) {
    return read_mfg(field, head, data, length, 1);
}

int mfg_write(char* field, struct mfg_entry** head, char* type, void* data, unsigned long length) {
#ifdef MFG_RW_FILE
    return write_mfg(field, head, type, data, length);
#else
    (void) field, (void) head, (void) type, (void) data, (void) length;
    return MFG_ERR_PERM;
#endif
}

char** mfg_getMfgEntryNames(struct mfg_entry* head, int* count) {
    return mfg_entries_getMfgEntryNames(head, count);
}
