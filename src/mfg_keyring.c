/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <kcapi.h>
#include <keyutils.h>
#include <openssl/bio.h>
#include <stdio.h>
#include <string.h>
#include "mfg_defs.h"
#include "mfg_keyring.h"
#include "mfg_logs.h"


// Function to convert a hex character to its integer value
static int hexchar_to_int(const char* hex_str, unsigned char* iv, size_t iv_len) {
    for(size_t i = 0; i < iv_len; i++) {
        char byte_str[3] = {hex_str[i * 2], hex_str[i * 2 + 1], '\0'}; // 2 hex chars + null terminator
        char* endptr = NULL;

        long byte_val = strtol(byte_str, &endptr, 16);
        if((*endptr != '\0') || (byte_val < 0) || (byte_val > 0xFF)) {
            ERROR("Error: Invalid character in IV file\n");
            return MFG_ERR_BAD_HEX;
        }

        iv[i] = (unsigned char) byte_val;
    }
    return 0;
}

// Function to parse IV and key descriptor from the file
int read_iv_from_file(const char* filename, unsigned char* iv, size_t iv_len) {
    if((filename == NULL) || (iv == NULL)) {
        ERROR("Received a NULL pointer while trying to open iv file");
        return MFG_ERR_IO;
    }
    FILE* file = fopen(filename, "r");
    if(!file) {
        ERROR("Error opening file %s\n", filename);
        return MFG_ERR_IO;
    }

    char hex_str[AES_BLOCK_SIZE * 2 + 1];  // Hex string to store IV (2 chars per byte)
    if(fgets(hex_str, sizeof(hex_str), file) == NULL) {
        ERROR("Failed to read IV from file");
        fclose(file);
        return MFG_ERR_BAD_HEX;
    }
    fclose(file);

    // Ensure the hex string length matches the expected IV size
    if(strlen(hex_str) != iv_len * 2) {
        ERROR("Error: IV file content length does not match expected size\n");
        return MFG_ERR_BAD_HEX;
    }

    if(hexchar_to_int(hex_str, iv, iv_len) != 0) {
        ERROR("Failed to convert the hex string to an integer");
        return MFG_ERR_BAD_HEX;
    }

    return 0;
}

// Function to decrypt AES-CBC data using the specified key and IV
int decrypt_aes_cbc(const unsigned char* encrypted_data, size_t encrypted_size,
                    const unsigned char* iv, const char* key_name,
                    unsigned char** data, size_t* length) {
    key_serial_t key_id = 0;
    struct kcapi_handle* handle = NULL;
    unsigned char* plaintext = NULL;

    if((encrypted_data == NULL) || (iv == NULL) || (key_name == NULL)) {
        ERROR("Received a NULL pointer in decrypt_aes_cbc");
        return MFG_ERR_KEYRING;
    }

    // Search for the combined key (AES key + IV) by name in the keyring
    key_id = keyctl_search(KEY_SPEC_USER_KEYRING, "logon", key_name, 0);
    if(key_id < 0) {
        ERROR("Failed to find the combined key in keyring");
        return MFG_ERR_KEYRING;
    }

    // Initialize a handle for the kcapi AES-CBC cipher
    if(kcapi_cipher_init(&handle, "cbc(aes-generic)", 0)) {
        ERROR("Failed to initialize kcapi handle\n");
        return MFG_ERR_KEYRING;
    }

    // Set the key from the keyring by its serial ID
    if(kcapi_cipher_setkey_by_key_serial(handle, key_id)) {
        ERROR("Could not set KRS key with serial\n");
        kcapi_cipher_destroy(handle);
        return MFG_ERR_KEYRING;
    }

    // Take the IV from the input data (first block size)
    size_t blocksize = kcapi_cipher_blocksize(handle);
    if(encrypted_size < blocksize) {
        ERROR("Encrypted data too small\n");
        kcapi_cipher_destroy(handle);
        return MFG_ERR_KEYRING;
    }

    if(blocksize != AES_BLOCK_SIZE) {
        ERROR("Invalid block size for cipherkey (expected %d, got %ld)",
              AES_BLOCK_SIZE, blocksize);
        kcapi_cipher_destroy(handle);
        return MFG_ERR_KEYRING;
    }

    // Allocate memory for the decrypted plaintext data
    plaintext = (unsigned char*) calloc(1, encrypted_size);
    if(plaintext == NULL) {
        ERROR("Error: Memory allocation failed for plaintext\n");
        kcapi_cipher_destroy(handle);
        return MFG_ERR_KEYRING;
    }

    // Perform the AES-CBC decryption operation using the specified key and IV
    ssize_t out_size = kcapi_cipher_decrypt(handle,
                                            encrypted_data, encrypted_size, //Encrypted input
                                            iv,                             // IV
                                            plaintext, encrypted_size,      // Output (plaintext buffer)
                                            KCAPI_ACCESS_HEURISTIC);        // Use heuristic access mode
    if(out_size < 0) {
        ERROR("Error: Decryption failed in kcapi\n");
        free(plaintext);
        kcapi_cipher_destroy(handle);
        return MFG_ERR_KEYRING;
    }

    *data = plaintext;
    *length = out_size;

    // Cleanup
    kcapi_cipher_destroy(handle);
    return 0;
}
