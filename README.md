# lib_mfg

[TOC]

# Introduction

lib_mfg is a library to interact with the MFG (Manufacturing Data),
also called SMD (Secure Manufacturing Data).
This API provides the ability to read that data.

# MFG library implementation

This library will create a linked list with all the fields from the MFG.

# Building, Installing and testing

## Building

### Prerequisites

<code>lib_mfg</code> depends on the following libraries:

- libsahtrace

These libraries can be installed in the container:

    sudo apt-get install mod-sahtrace sah-lib-sahtrace-dev

### Build lib_mfg
The module can be build in the PrplOS environment, or build locally on your machine.
When building locally, make sure to install the necessary prerequesites.

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    where the module should be stored and clone this library in it (on your
    local machine).

        cd ~/components
        git clone git@gitlab.com:prpl-foundation/components/mfgdata/libmfg.git

2. Build it

    You can build the package.

        cd ~/components/lib_mfg
        make

## Testing
There are unit tests for the non-secured fields. These tests run in the CI pipeline
### Prerequisites

The library and tests require `dtc` to be installed.

### Run tests
You can run the tests by executing the following command:

    cd test
    make

Or this command if you also want the coverage tests to run:

    cd tests
    make run coverage

You can combine both commands:

    cd ~/components/lib_mfg
    make test

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths
The documentation is split in two parts, starting from the repository path:

    cd ~/components/lib_mfg

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.

The code itself is documented in the approriate header and source files.
Documentation is only generated for the header files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/components/lib_mfg
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser.

#### Creating .pdf

For creating the pdf, an extra step is necessary.

    cd output/doc/doxy-latex
    make

After using `make`, the .pdf should be available in the directory.
## What's next?
- Parse a MFG .dts and create a linked list with all the fields

Features that could be implemented:
- [] Add support for Read Write RIP
- [] Reading from mtd device
- [] Support hex2bin (if necessary)
- [] Add Support for blacklisted items
